angular.module('usuarios.services', [])

.service('usuariosService',function ( $http, $q, CONFIG) {
    /*var user=JSON.parse(localStorage.getItem("userData"));*/
    this.list=function () {
      
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
              //      Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'allUsers',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;
    };
    
    this.get=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'usuarios/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    this.put=function(data){
        var deferred = $q.defer();
        $http({
	        method: "put",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:data,
            url: CONFIG.APIURL+'usuarios/'+data.id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
        
    };

     this.post=function(data){
        var deferred = $q.defer();
        $http({
	        method: "Post",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:data,
            url: CONFIG.APIURL+'usuarios',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
        
    };
    this.delete=function (id) {
        var deferred = $q.defer();
        $http({
	        method: "Delete",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
           
            url: CONFIG.APIURL+'usuarios/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
    };
})

.service('userService', function ($http, $q, CONFIG) {
   //var user=JSON.parse(localStorage.getItem("userData"));


    this.update=function(data){
       var deferred = $q.defer();
       $http({
           method:"put",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'usuarios/'+data.id,
           data:data
       }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
      }); 
     return deferred.promise;
    };
    
    this.getClient=function(idCliente, idEquipo){
       var deferred = $q.defer();
       $http({
           method:"get",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'allClientes/'+idCliente+'/'+idEquipo,
           
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
      }); 
     return deferred.promise;
    };
    
     this.get=function(idCliente){
       var deferred = $q.defer();
       $http({
           method:"get",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'clientes/'+idCliente,
           
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
      }); 
     return deferred.promise;
    };
})