angular.module('localizacion.services', [])

.service('localizacionService',function ( $http, $q, CONFIG) {
    /*var user=JSON.parse(localStorage.getItem("userData"));*/
    

    
    this.listDptos=function (id) {
      
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
              //      Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'paises/'+id+"/dptosEstados",
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;
    };
    
    
    this.listMpios=function (id) {
      
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
              //      Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'dptosEstado/'+id+"/mpiosCiudades",
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;
    };
    
    
    this.munucipio=function (id) {
      
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
              //      Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'mpiosCiudades/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;
    };
    
    
})