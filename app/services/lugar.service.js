angular.module('lugar.services', [])

.service('lugarService',function ( $http, $q, CONFIG) {
    /*var user=JSON.parse(localStorage.getItem("userData"));*/
    

    
    this.list=function (id) {
      
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
              //      Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'misLugares/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;
    };
    
    this.all=function () {
      
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
              //      Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'lugar',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;
    };
    
    
    this.get=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'lugar/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    
    this.canchasLugar=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'lugar/'+id+'/canchas',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    this.put=function(data){
        var deferred = $q.defer();
        $http({
	        method: "put",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:data,
            url: CONFIG.APIURL+'lugar/'+data.id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
        
    };
    
     this.post=function(data){
         
        var deferred = $q.defer();
        
        var formData = new FormData();
        //formData.append('fotos[]', data.fotos);
        var ins = data.fotos.length;
        for (var x = 0; x < ins; x++) {
            formData.append("fotos[]", data.fotos[x]._file);
            
        }

        formData.append('mail', data.mail);
        formData.append('direccion', data.direccion);
        formData.append('nombre', data.nombre);
        formData.append('telefono', data.telefono);
        formData.append('encargados_id', data.encargados_id);
        formData.append('admin_id', data.admin_id);
        formData.append('mpios_ciudades_id', data.mpios_ciudades_id);
        if(data.latitud!=undefined){formData.append('latitud', data.latitud);}
        if(data.longitud!=undefined){formData.append('longitud', data.longitud);}
        
       $http({
	        method: "Post",
	        headers:{'content-type':undefined,
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:formData,
            url: CONFIG.APIURL+'lugar',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
        
    };
    
    this.delete=function (id) {
        var deferred = $q.defer();
        $http({
	        method: "Delete",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
           
            url: CONFIG.APIURL+'lugar/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
    };
    
    
    this.deleteGallery=function (id) {
        var deferred = $q.defer();
        $http({
	        method: "Delete",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
           
            url: CONFIG.APIURL+'fotos_lugar/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
    };
    
    this.postGallery=function(data){
         
        var deferred = $q.defer();
        
        var formData = new FormData();
        //formData.append('fotos[]', data.fotos);
        var ins = data.fotos.length;
        for (var x = 0; x < ins; x++) {
            formData.append("fotos[]", data.fotos[x]._file);
            
        }
        formData.append('lugar_id', data.lugar_id);
        
        
       $http({
	        method: "Post",
	        headers:{'content-type':undefined,
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:formData,
            url: CONFIG.APIURL+'fotos_lugar',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
        
    };
    
    this.comentarios=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'comentarios/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    
})

.service('caracteristicasServices', function ($http, $q, CONFIG) {
   //var user=JSON.parse(localStorage.getItem("userData"));


    this.get=function(){
       var deferred = $q.defer();
       $http({
           method:"get",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'caracteristicas',
           
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
        }); 
        return deferred.promise;
    };
})

.service('searchService', function ($http, $q, CONFIG) {
   //var user=JSON.parse(localStorage.getItem("userData"));


    this.advanced=function(data){
       var deferred = $q.defer();
       $http({
           method:"post",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           data:data,
           url: CONFIG.APIURL+'advanceSearch',
           
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
        }); 
        return deferred.promise;
    };
})