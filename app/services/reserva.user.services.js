angular.module('reserva.user.services', [])

.service('reservasUserService', function ($http, $q, CONFIG) {
   //var user=JSON.parse(localStorage.getItem("userData"));


    this.postReto=function(data){
       var deferred = $q.defer();
       $http({
           method:"POST",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'reserva_reto',
           data:data
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
        }); 
     return deferred.promise;
    };
    
    this.postReservaRetoCalificacion=function(data){
        
       var deferred = $q.defer();
       $http({
           method:"POST",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'calificacionesRR',
           data:data
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
        }); 
     return deferred.promise;
    };
    
    
    this.putReserva=function(data){
       var deferred = $q.defer();
       $http({
           method:"Put",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'reservas/'+data.id,
           data:data
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
        }); 
     return deferred.promise;
    };
    
    this.putReservaReto=function(data){
       var deferred = $q.defer();
       $http({
           method:"Put",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'reserva_reto/'+data.id,
           data:data
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
        }); 
     return deferred.promise;
    };
    
    this.postReserva=function(data){
       var deferred = $q.defer();
       $http({
           method:"POST",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'reservas',
           data:data
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
        }); 
     return deferred.promise;
    };
    
    this.getReserva=function(id){
       var deferred = $q.defer();
       $http({
           method:"get",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'misReservasCliente/'+id,
           
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
        }); 
        return deferred.promise;
    };
    
    this.getReservaHistorial=function(id){
       var deferred = $q.defer();
       $http({
           method:"get",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'misReservasClienteHistorico/'+id,
           
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
        }); 
        return deferred.promise;
    };
    
    this.reserva=function(id){
       var deferred = $q.defer();
       $http({
           method:"get",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'reserva/'+id,
           
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
        }); 
        return deferred.promise;
    };
    
    this.reservaReto=function(id){
       var deferred = $q.defer();
       $http({
           method:"get",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'retosReserva/'+id,
           
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
        }); 
        return deferred.promise;
    };
    
     this.reservaRetoApp=function(id, user_id){
       var deferred = $q.defer();
       $http({
           method:"get",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'reserva_reto_app/'+id+"/"+user_id,
           
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
        }); 
        return deferred.promise;
    };
    
    
    this.getquienMeRetaReserva=function(id){
       var deferred = $q.defer();
       $http({
           method:"get",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'quienMeRetaReserva/'+id,
           
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
        }); 
        return deferred.promise;
    };
    
    this.getquienMeRetaReservaHistorial=function(id){
       var deferred = $q.defer();
       $http({
           method:"get",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'quienMeRetaReservaHistorico/'+id,
           
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
        }); 
        return deferred.promise;
    };

    this.getmisRetosReserva=function(id){
       var deferred = $q.defer();
       $http({
           method:"get",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'misRetosReserva/'+id,
           
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
        }); 
        return deferred.promise;
    };
    
    this.getmisRetosReservaHistorial=function(id){
       var deferred = $q.defer();
       $http({
           method:"get",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'misRetosReservaHistorico/'+id,
           
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
        }); 
        return deferred.promise;
    };
    
    this.retar=function (id) {
        var deferred = $q.defer();
       $http({
           method:"get",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'retar/'+id,
           
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
      }); 
     return deferred.promise;
    };
    
    this.getBancos=function(){
       var deferred = $q.defer();
       $http({
           method:"get",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'getBancos',
           
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
        }); 
        return deferred.promise;
    };
})