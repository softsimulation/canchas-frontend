angular.module('reserva.services', [])

.service('reservaService',function ( $http, $q, CONFIG) {
    /*var user=JSON.parse(localStorage.getItem("userData"));*/
    
    
    this.get=function (lugar_id, estado_id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'ReservasLugarEstado/'+lugar_id+'/'+estado_id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    this.getHistorico=function (lugar_id, estado_id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'ReservasLugarEstadoHistorico/'+lugar_id+'/'+estado_id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    this.edit=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'reservas/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    this.editReto=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'retosReservaPanel/'+id,
            }).success(function(result, status) {
                
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    this.put=function(data){
        var deferred = $q.defer();
        $http({
	        method: "put",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:data,
            url: CONFIG.APIURL+'reservas/'+data.id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
        
    };
    
    this.putReservas=function(data){
        var deferred = $q.defer();
        $http({
	        method: "Post",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:data,
            url: CONFIG.APIURL+'updateReserva',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
        
    };
    
    
    this.putReto=function(data){
        var deferred = $q.defer();
        $http({
	        method: "put",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:data,
            url: CONFIG.APIURL+'reserva_reto/'+data.id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
        
    };
    
    
     this.post=function(data){

        var deferred = $q.defer();
        $http({
	        method: "Post",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:data,
            url: CONFIG.APIURL+'reservas',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
        
    };
    
    this.estado=function() {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'estadoReserva',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
   
})