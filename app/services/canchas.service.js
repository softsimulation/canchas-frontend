angular.module('canchas.services', [])
.service('canchasService',function ( $http, $q, CONFIG) {
    /*var user=JSON.parse(localStorage.getItem("userData"));*/
    this.all=function () {
      
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
              //      Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'cancha',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;
    };
    
    this.list=function (id) {
      
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
              //      Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'misCanchas/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;
    };
    
    this.get=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'cancha/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    this.put=function(data){
        var deferred = $q.defer();
        $http({
	        method: "put",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:data,
            url: CONFIG.APIURL+'cancha/'+data.id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
        
    };
    
     this.post=function(data){
         
        var deferred = $q.defer();
        
        var formData = new FormData();
        //formData.append('fotos[]', data.fotos);
        var ins = data.fotos.length;
        for (var x = 0; x < ins; x++) {
            formData.append("fotos[]", data.fotos[x]._file);
            
        }
        
        var chip_l=data.chips.length;
        for (var i =0;i<chip_l;i++ ) {
            formData.append("chips[]", data.chips[i]);
        }
        
        
        formData.append('codigo', data.codigo);
        formData.append('caracteristicas', data.caracteristicas);
        formData.append('lugar_id', data.lugar_id);
        formData.append('disponibilidad_id', data.disponibilidad_id);
     
       $http({
	        method: "Post",
	        headers:{'content-type':undefined,
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:formData,
            url: CONFIG.APIURL+'cancha',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
        
    };
    
    this.delete=function (id) {
        var deferred = $q.defer();
        $http({
	        method: "Delete",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
           
            url: CONFIG.APIURL+'cancha/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
    };
    
    this.disponible=function () {
        var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
           
            url: CONFIG.APIURL+'disponibilidad',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
    };
    
    this.lugarCanchas=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'lugar/'+id+'/canchas',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    this.dias=function(){
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'dias',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;   
    };
    
    this.createHorario=function (data) {
       var deferred = $q.defer();
       $http({
	        method: "Post",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:data,
            url: CONFIG.APIURL+'horario',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;
    };
    
    this.getHorario=function (id) {
        var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'horario/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    this.updHorario=function (data) {
        var deferred = $q.defer();
        $http({
	        method: "put",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:data,
            url: CONFIG.APIURL+'horario/'+data.id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
    };
    
    this.delHorario=function (id) {
        var deferred = $q.defer();
        $http({
	        method: "delete",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'horario/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
    };
    
    this.deleteGallery=function (id) {
        var deferred = $q.defer();
        $http({
	        method: "Delete",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
           
            url: CONFIG.APIURL+'fotos/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
    };
    
    this.postGallery=function(data){
         
        var deferred = $q.defer();
        
        var formData = new FormData();
        //formData.append('fotos[]', data.fotos);
        var ins = data.fotos.length;
        for (var x = 0; x < ins; x++) {
            formData.append("fotos[]", data.fotos[x]._file);
            
        }
        formData.append('cancha_id', data.cancha_id);
        
        
       $http({
	        method: "Post",
	        headers:{'content-type':undefined,
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:formData,
            url: CONFIG.APIURL+'fotos',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
        
    };
    
});