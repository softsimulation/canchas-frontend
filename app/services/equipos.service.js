angular.module('equipos.services', [])
.service('equiposService',function ( $http, $q, CONFIG) {
    /*var user=JSON.parse(localStorage.getItem("userData"));*/
    
    
    
    this.get=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'misEquipos/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    this.getInactivos=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'misEquiposInactivos/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };

    
    
    this.equipo=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'miEquipo/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
   
    this.post=function (data) {
       
        var deferred = $q.defer();
       
        var formData = new FormData();
        //formData.append("logo", data.logo._file);
        if(data.logo && typeof(data.logo)!=="undefined"){formData.append("logo", data.logo._file);}
        formData.append("color", data.color);
        formData.append("nombre", data.nombre);
        formData.append("cliente_id", data.cliente_id);
        $http({
	        method: "Post",
	        headers:{'content-type':undefined,
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:formData,
            url: CONFIG.APIURL+'miEquipo',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
			return deferred.promise;
    
    };
    
    this.put=function (data) {
       
       var deferred = $q.defer();
       
        var formData = new FormData();
        if(data.logo && typeof(data.logo)!=="undefined"){formData.append("logo", data.logo._file);}
        formData.append("color", data.color);
        formData.append("nombre", data.nombre);
        formData.append("id", data.id);
        $http({
	        method: "POST",
	        headers:{'content-type':undefined,
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:formData,
            url: CONFIG.APIURL+'UpdMiEquipo',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
			return deferred.promise;
    
    };
    
    this.delete=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "delete",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'miEquipo/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    
    this.addJugadores=function (data) {
       
       var deferred = $q.defer();
        $http({
	        method: "POST",
	        header: {'content-type':'application/json'},
            url: CONFIG.APIURL+'addJugadores',
	        data: data
            }).success(function(result, status) {
               		  deferred.resolve(result);	
               		
            }).error(function(status, error) {
                		deferred.reject(error);
			});
			return deferred.promise;
    
   };
   
   this.jugadoresEquipo=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'jugadoresDeEquipo/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    this.jugadoresInvitados=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'jugadoresInvitados/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    
    this.quieroJugar=function (data) {
       
       var deferred = $q.defer();
        $http({
	        method: "POST",
	        header: {'content-type':'application/json'},
            url: CONFIG.APIURL+'quieroJugar',
	        data: data
            }).success(function(result, status) {
               		  deferred.resolve(result);	
               		
            }).error(function(status, error) {
                		deferred.reject(error);
			});
			return deferred.promise;
    
   };
   
   this.getQuieroJugar=function (id) {
       
       var deferred = $q.defer();
        $http({
	        method: "Get",
	        header: {'content-type':'application/json'},
            url: CONFIG.APIURL+'quieroJugar/'+id,

            }).success(function(result, status) {
               		  deferred.resolve(result);	
               		
            }).error(function(status, error) {
                		deferred.reject(error);
			});
			return deferred.promise;
    
   };
   
   
    this.jugar=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'quieroJugarcli/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    
       
    this.retar=function (data) {
       
       var deferred = $q.defer();
        $http({
	        method: "POST",
	        header: {'content-type':'application/json'},
            url: CONFIG.APIURL+'retar',
	        data: data
            }).success(function(result, status) {
               		  deferred.resolve(result);	
               		
            }).error(function(status, error) {
                		deferred.reject(error);
			});
			return deferred.promise;
    };
    
    
    this.meReta=function (id) {
       
       var deferred = $q.defer();
        $http({
	        method: "Get",
	        header: {'content-type':'application/json'},
            url: CONFIG.APIURL+'quienMeReta/'+id,
            }).success(function(result, status) {
               		  deferred.resolve(result);	
               		
            }).error(function(status, error) {
                		deferred.reject(error);
			});
			return deferred.promise;
    };


     this.updateReto=function (data) {
       
       var deferred = $q.defer();
        $http({
	        method: "PUT",
	        header: {'content-type':'application/json'},
            url: CONFIG.APIURL+'retar/'+data.id,
            data:data
            }).success(function(result, status) {
               		  deferred.resolve(result);	
               		
            }).error(function(status, error) {
                		deferred.reject(error);
			});
			return deferred.promise;
    };
    
    
    this.misRetos=function (id) {
       
       var deferred = $q.defer();
        $http({
	        method: "Get",
	        header: {'content-type':'application/json'},
            url: CONFIG.APIURL+'misRetos/'+id,
            }).success(function(result, status) {
               		  deferred.resolve(result);	
               		
            }).error(function(status, error) {
                		deferred.reject(error);
			});
			return deferred.promise;
    };
    
    
    
     this.deleteEquipoDef=function (data) {
       
        var deferred = $q.defer();
       
        
        $http({
	        method: "Post",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:data,
            url: CONFIG.APIURL+'deleteEquipoDef',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
			return deferred.promise;
    
    };
    
    this.restoreEquipo=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'restoreEquipo/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    this.estoyEnEquipo=function (id) {
       
       var deferred = $q.defer();
        $http({
	        method: "Get",
	        header: {'content-type':'application/json'},
            url: CONFIG.APIURL+'estoyEnEquipo/'+id,

            }).success(function(result, status) {
               		  deferred.resolve(result);	
               		
            }).error(function(status, error) {
                		deferred.reject(error);
			});
			return deferred.promise;
    
   };
   
   this.invitadoEnEquipo=function (id) {
       
       var deferred = $q.defer();
        $http({
	        method: "Get",
	        header: {'content-type':'application/json'},
            url: CONFIG.APIURL+'invitadoEnEquipo/'+id,

            }).success(function(result, status) {
               		  deferred.resolve(result);	
               		
            }).error(function(status, error) {
                		deferred.reject(error);
			});
			return deferred.promise;
    
   };
   
   
    this.updateJugadores=function (data) {
       
       var deferred = $q.defer();
        $http({
	        method: "PUT",
	        header: {'content-type':'application/json'},
            url: CONFIG.APIURL+'jugadores/'+data.id,
            data:data
            }).success(function(result, status) {
               		  deferred.resolve(result);	
               		
            }).error(function(status, error) {
                		deferred.reject(error);
			});
			return deferred.promise;
    };
    
    
    
})