angular.module('encargado.services', [])

.service('encargadoService',function ( $http, $q, CONFIG) {
    /*var user=JSON.parse(localStorage.getItem("userData"));*/
    this.list=function (id) {
      
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
              //      Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'misEncargados/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;
    };
    
    this.get=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'usuarios/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
    
    this.put=function(data){
        var deferred = $q.defer();
        $http({
	        method: "put",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:data,
            url: CONFIG.APIURL+'usuarios/'+data.id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
        
    };
    
     this.post=function(data){
        var deferred = $q.defer();
        $http({
	        method: "Post",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:data,
            url: CONFIG.APIURL+'usuarios',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
        
    };
    
    this.delete=function (id) {
        var deferred = $q.defer();
        $http({
	        method: "Delete",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
           
            url: CONFIG.APIURL+'usuarios/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
    };
    
    
})