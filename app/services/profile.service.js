angular.module('profile.user.service', [])
.service('profileUserService', function ($http, $q, CONFIG) {
   //var user=JSON.parse(localStorage.getItem("userData"));


    this.update=function(data){
       var deferred = $q.defer();
       $http({
           method:"put",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'usuarios/'+data.id,
           data:data
       }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
      }); 
     return deferred.promise;
    };
    
    this.getClient=function(idCliente, idEquipo){
       var deferred = $q.defer();
       $http({
           method:"get",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'allClientes/'+idCliente+'/'+idEquipo,
           
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
      }); 
     return deferred.promise;
    };
    
     this.get=function(idCliente){
       var deferred = $q.defer();
       $http({
           method:"get",
           headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
           },
           url: CONFIG.APIURL+'clientes/'+idCliente,
           
        }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
              
        }).error(function(status, error, data) {
                  deferred.reject(error);
      }); 
     return deferred.promise;
    };
})