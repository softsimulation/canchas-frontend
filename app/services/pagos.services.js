angular.module('pagos.services', [])
.service('pagoService', function ($http, $q, CONFIG) {
   this.postReserva=function (data) {
       
        var deferred = $q.defer();
        $http({
	        method: "POST",
	        header: {'content-type':'application/json'},
            url: CONFIG.APIURL+'pagoReserva',
	        data: data
            }).success(function(result, status) {
               		  deferred.resolve(result);	
               		
            }).error(function(status, error) {
                		deferred.reject(error);
			});
			return deferred.promise;
    
   };
   this.postReservaReto=function (data) {
       
        var deferred = $q.defer();
        $http({
	        method: "POST",
	        header: {'content-type':'application/json'},
            url: CONFIG.APIURL+'pagoReservaReto',
	        data: data
            }).success(function(result, status) {
               		  deferred.resolve(result);	
               		
            }).error(function(status, error) {
                		deferred.reject(error);
			});
			return deferred.promise;
    
   };  
})