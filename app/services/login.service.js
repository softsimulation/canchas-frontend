angular.module('login.services', [])

.service('loginService',function ( $http, $q, CONFIG) {
    /*var user=JSON.parse(localStorage.getItem("userData"));*/
    
    
    
    
     this.auth=function(data){
        var deferred = $q.defer();
        $http({
	        method: "Post",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:data,
            url: CONFIG.APIURL+'auth_login',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
        
    };
    this.userClient=function (id) {
       var deferred = $q.defer();
        $http({
	        method: "get",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            url: CONFIG.APIURL+'clienteByUser/'+id,
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise;  
    };
   
    
    
}).service('registerService', function ($http, $q, CONFIG) {
    this.post=function (data) {
      var deferred = $q.defer();
        $http({
	        method: "Post",
	        headers:{'content-type':'application/json',
                    //Authorization:'Bearer{'+user.token+'}'
            },
            data:data,
            url: CONFIG.APIURL+'usuarios',
            }).success(function(result, status) {
                //console.log(result)
                deferred.resolve(result);
					    
            }).error(function(status, error, data) {
                  deferred.reject(error);
	        }); 
	   return deferred.promise; 
   };
})