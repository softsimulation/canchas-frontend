angular.module('reserva.owner.controller', [])

.controller('createOwnerReserva', ['$scope', 'reservaService', '$routeParams', 'toastr', '$location','lugarService','canchasService', '$base64', '$cookieStore','adminService', function ($scope, reservaService, $routeParams, toastr, $location, lugarService, canchasService, $base64, $cookieStore, adminService) {
   
    $scope.reserva={};
    //var currentTime =;
    //$scope.reserva.fecha  =  ""+new Date();
    $scope.month = ['Enero ', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    $scope.monthShort = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
    $scope.weekdaysFull = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    $scope.weekdaysLetter = ['D', 'L', 'M', 'M', 'J', 'V', 'S'];
    $scope.today = 'Hoy';
    $scope.clear = 'Limpiar';
    $scope.close = 'Cerrar';
    
    var decoded=$base64.decode($cookieStore.get('userDat'));
    var user= JSON.parse(decoded);
    
    adminService.get(user.id).then(function(data){
         lugarService.list(data[0].id).then(function (data) {
            $scope.data=data;
            $scope.load=true;
            $(document).ready(function(){ $('select').material_select();});
        },function (error, data) {
             console.log(error);
        });
       
    },function (error, data) {
            console.log(error);
      
    });
    
    
 
    $scope.lugar_get=function (id) {
        //console.log(id);
        
        canchasService.lugarCanchas(id).then(function (data) {
            $scope.cancha=data;
            $scope.load=true;
            $(document).ready(function(){ $('select').material_select();});
        },function (error, data) {
             console.log(error);
        });
    
    };
    
    $scope.create=function (reserva) {
        //console.log(reserva);
        var reservas={};
        
        
        
        if(!reserva){
             toastr.warning('Debes completar todos los campos', 'Alerta'); 
        }
        else if(!reserva.fecha || !reserva.horaFin || !reserva.horaInicio || !reserva.cancha_id){
             toastr.warning('Debes completar todos los campos', 'Alerta'); 
        }
        else{
            if(reserva.horaInicio>=reserva.horaFin){
                 toastr.warning('La hora final es menor o igual a la inicial', 'Alerta');
            
            }else{
                var currentDate=new Date();
                var date=new Date();
                var dates=reserva.fecha.split("/");
                date.setFullYear(dates[2], dates[1]-1, dates[0]);
                if(date<currentDate){
                    toastr.warning('La fecha de su reserva es menor a la actual', 'Alerta');
                }
                else{
                   
                    
                    reservas.fecha=date.getFullYear()+"-"+addZero(date.getMonth()+1)+"-"+addZero(date.getDate());
                    reservas.horaInicio=reserva.horaInicio/*getHours()+":"+reserva.horaInicio.getMinutes()*/;
                    reservas.horaFin=reserva.horaFin/*getHours()+":"+reserva.horaFin.getMinutes()*/;
                    reservas.cancha_id=reserva.cancha_id;
                    reservas.usuarios_id=user.id;
                    reservas.estado_reserva_id=4;
                    
                    var url='/owner/lugares/'+reserva.lugar_id+'/reserva';
                    toastr.info('Espere por favor...', 'Creando...');
                    reservaService.post(reservas).then(function (data) {
                        reserva={};
                        
                        if(data.estado==="fail"){
                            toastr.warning(data.mensaje, 'Alerta');
                        }else{
                            toastr.success('Se ha creado correctamente', 'Exito');
                       
                            $location.path(url); 
                        }
                    
                       
                      
                    },
                    function (argument) {
                        $scope.load=false;
                        toastr.error('Ha ocurrido un error grave', 'Error'); 
                          
                    });
                
                }
                
            }
        }
    };
    
    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    
}])

.controller('listOwnerCanchasReserva', ['$scope', 'reservaService', '$routeParams', 'toastr', '$location','lugarService','canchasService', '$base64', '$cookieStore', 'adminService', function ($scope, reservaService, $routeParams, toastr, $location, lugarService, canchasService, $base64, $cookieStore, adminService) {
    var decoded=$base64.decode($cookieStore.get('userDat'));
    var user= JSON.parse(decoded);
    adminService.get(user.id).then(function(data){
         lugarService.list(data[0].id).then(function (data) {
            $scope.data=data;
            $scope.load=true;
            $(document).ready(function(){ $('select').material_select();});
        },function (error, data) {
             console.log(error);
        });
       
    },function (error, data) {
            console.log(error);
      
    });
 
}])

.controller('getOwnerReserva', ['$scope', 'reservaService', '$routeParams', 'toastr', '$location','lugarService','canchasService', function ($scope, reservaService, $routeParams, toastr, $location, lugarService, canchasService) {
  
    $(document).ready(function(){ $('.modal').modal();});
    $scope.sortType     = ''; 
    $scope.sortReverse  = true;  
    ///$scope.searchFish   = '';     
    $scope.currentPage = 0;
    $scope.pageSize = 10;           
   
    $scope.numberOfPages=function(){
        //console.log(Math.ceil($scope.data.length/$scope.pageSize))
        return Math.ceil($scope.data.length/$scope.pageSize);                
    };
    
    $scope.pagination=function(page){
        $scope.currentPage=page;    
    };
   
    reservaService.get($routeParams.id, 0).then(function (data) {
        //console.log($routeParams.estado)
        $scope.data=data;
        //$scope.data2=data[1];
        
        $scope.numberOfPages();
        
        $scope.load=true;
    },function (error, data) {
         console.log(error);
    });
    
    reservaService.estado().then(function (data) {
        $scope.estados=data;
        $(document).ready(function(){ $('select').material_select();});
    },function (error, data) {
         console.log(error);
    });
    
    $scope.status=function (id) {
        $scope.load=false;
        if(id==""){
            reservaService.get($routeParams.id, 0).then(function (data) {
                //console.log($routeParams.estado)
                $scope.data=data;
                $scope.numberOfPages();
                //$scope.data2=data[1];
                $scope.load=true;
            },function (error, data) {
                 console.log(error);
            });
            
        }else{
            
            reservaService.get($routeParams.id, id).then(function (data) {
                //console.log($routeParams.estado)
                $scope.data=data;
                $scope.numberOfPages();
                $scope.load=true;
            },function (error, data) {
                 console.log(error);
            });
        }
    };
   
   
}])

.controller('getOwnerReservaHisto', ['$scope', 'reservaService', '$routeParams', 'toastr', '$location','lugarService','canchasService', function ($scope, reservaService, $routeParams, toastr, $location, lugarService, canchasService) {
   
   
    $scope.sortType     = ''; 
    $scope.sortReverse  = true;  
    ///$scope.searchFish   = '';     
    $scope.currentPage = 0;
    $scope.pageSize = 10;           
   
    $scope.numberOfPages=function(){
        //console.log(Math.ceil($scope.data.length/$scope.pageSize))
        return Math.ceil($scope.data.length/$scope.pageSize);                
    };
    
    $scope.pagination=function(page){
        $scope.currentPage=page;    
    };
   
    reservaService.getHistorico($routeParams.id, 0).then(function (data) {
        //console.log($routeParams.estado)
        $scope.data=data;
        //$scope.data2=data[1];
        
        $scope.numberOfPages();
        
        $scope.load=true;
    },function (error, data) {
         console.log(error);
    });
    
    reservaService.estado().then(function (data) {
        $scope.estados=data;
        $(document).ready(function(){ $('select').material_select();});
    },function (error, data) {
         console.log(error);
    });
    
    $scope.status=function (id) {
        $scope.load=false;
        if(id==""){
            reservaService.getHistorico($routeParams.id, 0).then(function (data) {
                //console.log($routeParams.estado)
                $scope.data=data;
                $scope.numberOfPages();
                //$scope.data2=data[1];
                $scope.load=true;
            },function (error, data) {
                 console.log(error);
            });
            
        }else{
            
            reservaService.getHistorico($routeParams.id, id).then(function (data) {
                //console.log($routeParams.estado)
                $scope.data=data;
                $scope.numberOfPages();
                $scope.load=true;
            },function (error, data) {
                 console.log(error);
            });
        }
    };
   
   
}])

.controller('editOwnerReserva', ['$scope','$route','$timeout', 'reservaService', '$routeParams', 'toastr', '$location','lugarService','canchasService', '$interval', function ($scope, $route,$timeout, reservaService, $routeParams, toastr, $location, lugarService, canchasService, $interval) {
    $scope.showDate=true;
   
    /*$interval(test, 5000);
    function test() {
       console.log("vale monda");
    }*/
   
   
    $scope.month = ['Enero ', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    $scope.monthShort = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
    $scope.weekdaysFull = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    $scope.weekdaysLetter = ['D', 'L', 'M', 'M', 'J', 'V', 'S'];
    $scope.today = 'Hoy';
    $scope.clear = 'Limpiar';
    $scope.close = 'Cerrar';
    $scope.reverse=function () {
        $scope.showDate=!$scope.showDate;
    };
   
    reservaService.edit($routeParams.id).then(function (data) {
        $scope.data=data;
        $scope.load=true;

        lugarService.get(data.cancha.lugar_id).then(function (datas) {
            $scope.lugar=datas;
            $scope.load=true;
          
        },function (error, data) {
             console.log(error);
        });
        
        canchasService.lugarCanchas(data.cancha.lugar_id).then(function (datas) {
            $scope.cancha=datas;
            $scope.cancha_id=""+data.cancha.id;
            $(document).ready(function(){ $('select').material_select();});
        },function (error, data) {
             console.log(error);
        });
        
        
    },function (error, data) {
         console.log(error);
    });
    
    reservaService.estado().then(function (data) {
        $scope.estados=data;
    },function (error, data) {
         console.log(error);
    });
    
    
    $scope.update=function (estado_id) {
        var reserva={};
        
        reserva.id=$routeParams.id;
        reserva.estado_reserva_id=estado_id;
        
        toastr.info('Espere por favor...', 'Actualizando...');
        reservaService.put(reserva).then(function (data) {
            
            toastr.success('Se ha actualizado correctamente', 'Exito');
            $location.path('/owner/reserva/listado/'); 
        },
        function (argument) {
            $scope.load=false;
            toastr.error('Ha ocurrido un error grave', 'Exito'); 
              
        });
        
    };
    
    $scope.updateDate=function(horaInicio, horaFin, fecha, cancha_id){
        var reservas={};
        
        var horai=horaInicio.split(":");
        var horaf=horaFin.split(":");
        horaInicio=horai[0]+":"+horai[1];
        horaFin=horaf[0]+":"+horaf[1];
        
        if(horaInicio>=horaFin){
             toastr.warning('La hora final es menor o igual a la inicial', 'Alerta');
        
        }else{
            var currentDate=new Date();
            var date=new Date();
            var dates=fecha.split("-");
            date.setFullYear(dates[0], dates[1]-1, dates[2]);
           
            if(date<currentDate){
                toastr.warning('La fecha de su reserva es menor a la actual', 'Alerta');
            }
            else{
               
                
                reservas.fecha=date.getFullYear()+"-"+addZero(date.getMonth()+1)+"-"+addZero(date.getDate());
                reservas.horaInicio=horaInicio;
                reservas.horaFin=horaFin;
                reservas.id=$routeParams.id;
                reservas.cancha_id=cancha_id;
                //console.log(reservas);
                //var url='/admin/lugares/'+reserva.lugar_id+'/reserva';
                toastr.info('Espere por favor...', 'Creando...');
                reservaService.putReservas(reservas).then(function (data) {
                    
                    if(data.estado==="fail"){
                        toastr.warning(data.mensaje, 'Alerta');
                    }else{
                        toastr.success('Se ha creado correctamente', 'Exito');
                        $(document).ready(function(){ $('.modal').modal('close');});
                        $timeout(function() {
                                 $route.reload();
                        }, 3000);
                       
                 
                    }
                
                   
                  
                },
                function (argument) {
                    $scope.load=false;
                    toastr.error('Ha ocurrido un error grave', 'Error'); 
                      
                });
            }
        }
    };
    
    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

   
}])

.controller('editOwnerReservaReto', ['$scope', 'reservaService', '$routeParams', 'toastr', '$location','lugarService','canchasService', '$interval', function ($scope, reservaService, $routeParams, toastr, $location, lugarService, canchasService, $interval) {
   
    
    /*$interval(test, 5000);
    function test() {
       console.log("vale monda");
    }*/
   
    reservaService.editReto($routeParams.id).then(function (data) {
        $scope.data=data[0];
        $scope.load=true;
        
        lugarService.get(data[0].lugar_id).then(function (datas) {
            $scope.lugar=datas;
            $scope.load=true;
          
        },function (error, data) {
             console.log(error);
        });
        
        
    },function (error, data) {
         console.log(error);
    });
    
    reservaService.estado().then(function (data) {
        $scope.estados=data;
    },function (error, data) {
         console.log(error);
    });
    
    
    $scope.update=function (estado_id) {
        var reserva={};
        
        reserva.id=$routeParams.id;
        reserva.estado_reserva_id=estado_id;
        
        toastr.info('Espere por favor...', 'Actualizando...');
        reservaService.putReto(reserva).then(function (data) {
            
            toastr.success('Se ha actualizado correctamente', 'Exito');
            $location.path('/owner/reserva/listado/'); 
        },
        function (argument) {
            $scope.load=false;
            toastr.error('Ha ocurrido un error grave', 'Exito'); 
              
        });
        
    };
    
   
}]);