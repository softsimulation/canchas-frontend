angular.module('reserva.user.controller', [])

.controller('misReservasController', ['$scope', '$location', 'reservasUserService', '$base64', '$cookieStore',function($scope, $location, reservasUserService, $base64, $cookieStore ) {
    var decoded=$base64.decode($cookieStore.get('userDat'));
    var user= JSON.parse(decoded);
    $scope.url="https://canchas-deportivas-jeferbustamante.c9users.io/canchas/";
    

    reservasUserService.getReserva(user.id).then(function (data) {
        $scope.reservas=data;
    }, 
    function (error, data) {});
    
    
    reservasUserService.getquienMeRetaReserva(user.cliente.id).then(function (data) {
       
        $scope.meReta=data;
       
    }, 
    function (error, data) {});
    
    reservasUserService.getmisRetosReserva(user.cliente.id).then(function (data) {
        $scope.misRetos=data;
    }, 
    function (error, data) {});  
}])

.controller('historicoController',['$scope', '$location', 'reservasUserService', '$base64', '$cookieStore', function($scope,$location,reservasUserService, $base64, $cookieStore ) {
    var decoded=$base64.decode($cookieStore.get('userDat'));
    var user= JSON.parse(decoded);
    
    $scope.url="https://canchas-deportivas-jeferbustamante.c9users.io/canchas/";
 
    reservasUserService.getReservaHistorial(user.id).then(function (data) {
        
        $scope.reservas=data;
    }, 
    function (error, data) {});
    
    
    reservasUserService.getquienMeRetaReservaHistorial(user.cliente.id).then(function (data) {
        
        $scope.meReta=data;
    }, 
    function (error, data) {});
    
    reservasUserService.getmisRetosReservaHistorial(user.cliente.id).then(function (data) {
        
        $scope.misRetos=data;
    }, 
    function (error, data) {});   
}])

.controller('reservarController',['$scope', 'lugarService', '$location', 'reservasUserService', '$base64', '$cookieStore', 'toastr',function($scope, lugarService, $location, reservasUserService, $base64, $cookieStore,toastr ) {
    
    $scope.month = ['Enero ', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    $scope.monthShort = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
    $scope.weekdaysFull = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    $scope.weekdaysLetter = ['D', 'L', 'M', 'M', 'J', 'V', 'S'];
    $scope.today = 'Hoy';
    $scope.clear = 'Limpiar';
    $scope.close = 'Cerrar';
    
    
    var decoded=$base64.decode($cookieStore.get('userDat'));
    var user= JSON.parse(decoded);
    
    $scope.url="https://canchas-deportivas-jeferbustamante.c9users.io/canchas/";
 
    lugarService.all().then(function (data) {
       
        $scope.lugares=data;     
        $(document).ready(function(){ $('select').material_select();});
    }, 
    function (error, data) {});


    $scope.change=function(lugar_id){
        lugarService.canchasLugar(lugar_id).then(function (data) {
            $scope.canchas=data;
            $(document).ready(function(){ $('select').material_select();});
        },function (error, data) {});
    };
    
    
    $scope.add=function (data) {
        
        var dat={
            'estado_reserva_id':3,
            'horaInicio':data.horaInicio,
            'horaFin':data.horaFinal,
            'cancha_id':data.cancha_id,
            'usuarios_id':user.id,
            'fecha':data.fecha,
            
        };
        if(dat.horaFin <= dat.horaInicio){
             toastr.warning('La hora Inicial no puede ser mayor o iguala la final', 'Alerta');
        }else{
           
            reservasUserService.postReserva(dat).then(function (data) {
               if(data.estado==="fail"){
                    toastr.warning(data.mensaje, 'Alerta');
               }else{
                    toastr.success(data.mensaje, 'Exito');
                    $location.path('user/misReservas'); 
               }
            },
            function (error) {});
        }
    };
}])

.controller('reservaController',['$scope', 'reservasUserService', '$location', 'localizacionService', 'pagoService', '$base64', '$cookieStore', '$routeParams', '$route', 'toastr', '$timeout', function($scope,reservasUserService, $location, localizacionService, pagoService, $base64, $cookieStore, $routeParams, $route, toastr, $timeout) {
    
    var decoded=$base64.decode($cookieStore.get('userDat'));
    var user= JSON.parse(decoded);
    $scope.load=false;
    $scope.data={formaPago:"tarjeta"};
    $scope.paymentMethod=function(method){$scope.data.formaPago=method;};
    
    $(document).ready(function(){ $('.modal').modal();});  

    reservasUserService.reserva($routeParams.id).then(function (data) {
        $scope.load=true;
        $scope.item=data;
    }, 
    function (error, data) {
        toastr.error('Ha ocurrido un error grave', 'Error'); 
    });
    
    
    reservasUserService.getBancos().then(function (data) {
        $scope.bancos=data;
        $(document).ready(function(){ $('select').material_select();});
    }, 
    function (error, data) {
        toastr.error('Ha ocurrido un error grave', 'Error'); 
    });
    
    
    localizacionService.listDptos(1).then(function (data) {
        $scope.dptos=data;
        $(document).ready(function(){ $('select').material_select();});
    }, 
    function (error, data) {
        toastr.error('Ha ocurrido un error grave', 'Error'); 
    });
    
    
    $scope.municiposload=function(id){
       
        localizacionService.listMpios(id).then(function (data) {
            $scope.mpios=data;
            $(document).ready(function(){ $('select').material_select();});
            
        }, function (error, data) {
            toastr.error('Ha ocurrido un error grave', 'Error'); 
        }); 
    };
    
    
   
    $scope.calificar=function (rating) {
        
        
        rating.id=$routeParams.id;
        toastr.info('Espere por favor...', 'Calificando...');
        reservasUserService.putReserva(rating).then(function (data) {
            $(document).ready(function(){ $('.modal').modal('close');});
            toastr.success('Se ha enviado su calficacion', 'Exito');
            $timeout(function(){$route.reload();},500);
        }, 
        function (error, data) {
            toastr.error('Ha ocurrido un error grave', 'Error'); 
        });
    };
    
    $scope.pagar=function (payment) {
        
        payment.user=user.id;
        payment.TotalPagar=$scope.item.precio;
        payment.reserva_id=$routeParams.id;
        
        toastr.info('Espere por favor...', 'Generando pago...');
        pagoService.postReserva(payment).then(function (data) {
            
            $(document).ready(function(){ $('.modal').modal('close');});
            if(payment.formaPago==="PSE"){
                toastr.success('Se ha enviado la url de pago a su email', 'Exito');
            }else{
                toastr.success(data.Respuesta.response, 'Exito');
            }
             
            $timeout(function(){$route.reload();},500);
            
        }, function (error, data) {
            toastr.error('Ha ocurrido un error grave', 'Error'); 
        });
        
    }; 
}])

.controller('retoReservadoController',[ '$scope' ,'reservasUserService', '$location','localizacionService', 'pagoService', '$base64', '$cookieStore', '$routeParams', '$route', 'toastr', '$timeout', function($scope ,reservasUserService, $location,localizacionService, pagoService, $base64, $cookieStore, $routeParams, $route, toastr, $timeout) {
    var decoded=$base64.decode($cookieStore.get('userDat'));
    var user= JSON.parse(decoded);
    $scope.load=false;
    $scope.data={formaPago:"tarjeta"};
    $scope.paymentMethod=function(method){$scope.data.formaPago=method;};
    $(document).ready(function(){ $('.modal').modal();});  
    $scope.url="https://canchas-deportivas-jeferbustamante.c9users.io/canchas/";
    
    reservasUserService.reservaRetoApp($routeParams.id, user.cliente.id).then(function (data) {
        
        $scope.item=data[0];
        $scope.load=true;
    }, 
    function (error, data) {
       
        
    });
    
    reservasUserService.getBancos().then(function (data) {
        $scope.bancos=data;
        $(document).ready(function(){ $('select').material_select();});
    }, 
    function (error, data) {
        toastr.error('Ha ocurrido un error grave', 'Error');     
    });
    
    
    localizacionService.listDptos(1).then(function (data) {
        $scope.dptos=data;
        $(document).ready(function(){ $('select').material_select();});
    }, 
    function (error, data) {
        toastr.error('Ha ocurrido un error grave', 'Error'); 
    });
    
    
    $scope.municiposload=function(id){
        
        localizacionService.listMpios(id).then(function (data) {
            $scope.mpios=data;
            $(document).ready(function(){ $('select').material_select();});
        }, function (error, data) {
            toastr.error('Ha ocurrido un error grave', 'Error'); 
        }); 
    };
    
    $scope.calificar=function (rating) {
        
        
        rating.reserva_reto_id=$routeParams.id;
        rating.cliente_id=user.cliente.id;
        toastr.info('Espere por favor...', 'Calificando...');
        reservasUserService.postReservaRetoCalificacion(rating).then(function (data) {
            $(document).ready(function(){ $('.modal').modal('close');});
            toastr.success('Se ha enviado su calficacion', 'Exito');
            $timeout(function(){$route.reload();},500);
        }, 
        function (error, data) {
            toastr.error('Ha ocurrido un error grave', 'Error'); 
        });
    };
    

    $scope.pagar=function (payment) {
        
        payment.user=user.id;
        payment.TotalPagar=$scope.item.precio;
        payment.reserva_id=$routeParams.id;
        toastr.info('Espere por favor...', 'Generando pago...');
        pagoService.postReservaReto(payment).then(function (data) {
            $(document).ready(function(){ $('.modal').modal('close');});
            if(payment.formaPago==="PSE"){
                toastr.success('Se ha enviado la url de pago a su email', 'Exito');
            }else{
                toastr.success(data.Respuesta.response, 'Exito');
            }
             
            $timeout(function(){$route.reload();},500);
        }, function (error, data) {
            toastr.error('Ha ocurrido un error grave', 'Error'); 
        });
        
    };    
}])

.controller('reservaRetoController', ['$scope', 'lugarService', 'userService', 'equiposService', '$location', 'reservasUserService', '$base64', '$cookieStore', '$routeParams', 'toastr', function($scope, lugarService, userService, equiposService, $location, reservasUserService, $base64, $cookieStore, $routeParams, toastr) {
    var decoded=$base64.decode($cookieStore.get('userDat'));
    var user= JSON.parse(decoded);
    $scope.url="https://canchas-deportivas-jeferbustamante.c9users.io/canchas/";
    

    reservasUserService.retar($routeParams.id).then(function (data) {
        $scope.item=data[0];
        lugarService.canchasLugar($scope.item.lugar_id).then(function (data) {
            $scope.canchas=data;
        
        },function (error, data) {
            toastr.error('Ha ocurrido un error grave', 'Error'); 
        });

    }, function () {
        toastr.error('Ha ocurrido un error grave', 'Error'); 
    });
    
    
    
    $scope.add=function (data) {
        
    
        var dat={
            'retar_id':$routeParams.id,
            'estado_reserva_id':3,
            'horaInicio':data.horaInicio,
            'horaFin':data.horaFinal,
            'cancha_id':data.cancha_id,
            'usuarios_id':user.id,
            'fecha':$scope.item.fecha,
            
        };
       
        if(dat.horaFin <= dat.horaInicio){
            
            toastr.warning('La hora Inicial no puede ser mayor o iguala la final', 'Alerta');
        }else{
           reservasUserService.postReto(dat).then(function (data) {
    
               if(data.estado==="fail"){
                    toastr.warning(data.mensaje, 'Alerta');
                    
               }else{
                    toastr.success(data.mensaje, 'Exito');
                    $location.path('user/misReservas'); 
               }
           },
           function (error) {
                toastr.error('Ha ocurrido un error grave', 'Error');
           }); 
            
        }
    };
    
    
}])


