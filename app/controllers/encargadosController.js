angular.module('encargado.controller', [])


.controller('listEncargado', ['$scope', 'encargadoService', '$cookieStore','$base64', 'adminService',function ($scope, encargadoService, $cookieStore, $base64, adminService) {
    $scope.load=false;
    var decoded=$base64.decode($cookieStore.get('userDat'));
    var user= JSON.parse(decoded);
    
    adminService.get(user.id).then(function(data){
        encargadoService.list(data[0].id).then(function (data) {
        $scope.data=data;
        $scope.load=true;
    }, 
    function (error, data) {
        console.log(error);
    });
       
    },function (error, data) {
            console.log(error);
      
    });
    
    
    
}])
.controller('viewEncargado', ['$scope', 'encargadoService', '$routeParams', function ($scope, encargadoService, $routeParams) {
    $scope.load=false;
    encargadoService.get($routeParams.id).then(function (data) {
        $scope.data=data;
        $scope.load=true;
    },function (error, data) {
         console.log(error);
    });
    
}])

.controller('editEncargado', ['$scope', 'encargadoService', '$routeParams', 'toastr', '$location', function ($scope, encargadoService, $routeParams, toastr, $location) {
    $scope.load=false;
    encargadoService.get($routeParams.id).then(function (data) {
        $scope.user=data;
        $scope.load=true;
    },function (error, data) {
         console.log(error);
    });
    
    
    $scope.update=function (user) {
        
        toastr.info('Espere por favor...', 'Actualizando...');
        encargadoService.put(user).then(function (data) {
            toastr.success('Se ha actualizado correctamente', 'Exito');
            $location.path('/owner/encargados'); 
        },
        function (argument) {
            $scope.load=false;
            toastr.error('Ha ocurrido un error grave', 'Exito'); 
              
        });
    };
    
}])
.controller('createEncargado', ['$scope', 'encargadoService', '$routeParams', 'toastr', '$location', '$base64', '$cookieStore', 'adminService', function ($scope, encargadoService, $routeParams, toastr, $location, $base64, $cookieStore, adminService) {
    
    $scope.load=false;
    var decoded=$base64.decode($cookieStore.get('userDat'));
    var user= JSON.parse(decoded);
    
    adminService.get(user.id).then(function(data){
        $scope.admin=data[0];
           
    },function (error, data) {
            console.log(error);
      
    });
    $scope.create=function (user) {
        user.tipo=2;
        user.admin_id=$scope.admin.id;
        toastr.info('Espere por favor...', 'Creando...');
        encargadoService.post(user).then(function (data) {
            user={};
            toastr.success('Se ha creado correctamente', 'Exito');
            $location.path('/owner/encargados'); 
        },
        function (argument) {
            $scope.load=false;
            toastr.error('Ha ocurrido un error grave', 'Exito'); 
              
        });
    };
    
}])
.controller('deleteEncargado', ['$scope', 'encargadoService', '$routeParams', 'toastr', '$location', function ($scope, encargadoService, $routeParams, toastr, $location) {
    $scope.load=false;
    encargadoService.get($routeParams.id).then(function (data) {
        $scope.data=data;
        $scope.load=true;
    },function (error, data) {
         console.log(error);
    });
    
    
    $scope.delete=function () {
        
        toastr.warning('Espere por favor...', 'Eliminando...');
        encargadoService.delete($routeParams.id).then(function (data) {
            toastr.success('Eliminado correctamente', 'Exito');
            $location.path('/owner/encargados'); 
        },
        function (argument) {
            $scope.load=false;
            toastr.error('Ha ocurrido un error grave', 'Exito'); 
              
        });
    };
    
}])

;
