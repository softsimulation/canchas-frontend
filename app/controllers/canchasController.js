angular.module('canchas.controller', [])

.controller('createCancha', ['$scope', 'canchasService', '$routeParams', 'toastr', '$location','lugarService', function ($scope, canchasService, $routeParams, toastr, $location, lugarService) {
    
    lugarService.all().then(function (data) {
        $scope.data=data;
        $scope.load=true;
       $(document).ready(function(){ $('select').material_select();});
    },function (error, data) {
         console.log(error);
    });
    
    canchasService.disponible().then(function (data) {
        $scope.disp=data;
        $scope.load=true;
        $(document).ready(function(){ $('select').material_select();});
    },function (error, data) {
         console.log(error);
    });
    
    
    $scope.create=function (cancha) {
        toastr.info('Espere por favor...', 'Creando...');
        canchasService.post(cancha).then(function (data) {
            cancha={};
            toastr.success('Se ha creado correctamente', 'Exito');
            $location.path('/admin/canchas');
           
        },
        function (argument) {
            $scope.load=false;
            toastr.error('Ha ocurrido un error grave', 'Exito'); 
              
        });
    };
    
}])
.controller('listCanchas', ['$scope', 'canchasService',function ($scope, canchasService) {
    $scope.load=false;
    canchasService.all().then(function (data) {
        $scope.data=data;
        $scope.load=true;
    }, 
    function (error, data) {
        console.log(error);
    });
    
}])
.controller('viewCanchas', ['$scope', 'canchasService', '$routeParams', function ($scope, canchasService, $routeParams) {
    $scope.load=false;
    
    canchasService.get($routeParams.id).then(function (data) {
        $scope.data=data;
        $scope.load=true;
    },function (error, data) {
         console.log(error);
    });
    
}])
.controller('createHorario', ['$scope', 'canchasService', '$routeParams', 'toastr', 'SweetAlert',function ($scope, canchasService, $routeParams, toastr, SweetAlert) {
    
    $(document).ready(function(){ $('.modal').modal();});
    $scope.sideBar='';
    canchasService.get($routeParams.id).then(function (data) {
        $scope.data=data.horario;
        //console.log(data.horario)
        $scope.load=true;
    },function (error, data) {
         console.log(error);
    });
    
   
    canchasService.dias().then(function (data) {
        $scope.dias=data;
        $(document).ready(function(){ $('select').material_select();});
    },function (error, data) {
         console.log(error);
    });
    
    
    $scope.send=function (horario) {
        //console.log(horario);
        if(!horario){
             toastr.warning('Debes completar todos los campos', 'Alerta'); 
        }else if((horario.horaInicio==""|| !horario.horaInicio) || (horario.horaFin==""|| !horario.horaFin) || 
                 (horario.precio==""|| !horario.precio) || (horario.dias_id==""|| !horario.dias_id))
        {
              toastr.warning('Debes completar todos los campos', 'Alerta'); 
        }else{
            if(horario.horaInicio>=horario.horaFin){
                toastr.warning('La hora final es menor o igual a la inicial', 'Alerta');
            }else{
                horario.cancha_id=$routeParams.id;
                //console.log(horario);
                canchasService.createHorario(horario).then(function (data) {
                    if(data.estado=='Ok'){
                        horario.horaInicio="";
                        horario.horaFin="";
                        horario.precio="";
                        horario.dias_id="";
                        canchasService.get($routeParams.id).then(function (data) {
                            $scope.data=data.horario;
                            $(document).ready(function(){ $('.modal').modal('close');});
                        },function (error, data) {
                             console.log(error);
                        });
                        
                        toastr.success('Se ha creado correctamente', 'Exito');
                            
                    }else{
                        toastr.warning(data.mensaje, 'Alerta');
                        
                    }
                    
                },function (error, data) {
                     console.log(error);
                });
            }
        }
    } ;
    
    $scope.modalUpdate=function (id) {
        
        canchasService.getHorario(id).then(function (data) {
            $scope.horarioUpd=data;
            $scope.horarioUpd.dias_id=""+data.dias_id;
            $(document).ready(function(){ $('select').material_select();});
        },function (error, data) {
            console.log(error);
        });
    };
  
  
    $scope.update=function (horarioUpd) { 
        if(!horarioUpd){
             toastr.warning('Debes completar todos los campos', 'Alerta'); 
        }else if((horarioUpd.horaInicio==""|| !horarioUpd.horaInicio) || (horarioUpd.horaFin==""|| !horarioUpd.horaFin) || 
                 (horarioUpd.precio==""|| !horarioUpd.precio) || (horarioUpd.dias_id==""|| !horarioUpd.dias_id))
        {
              toastr.warning('Debes completar todos los campos', 'Alerta'); 
        }else{
            if(horarioUpd.horaInicio>=horarioUpd.horaFin){
                toastr.warning('La hora final es menor o igual a la inicial', 'Alerta');
            }else{
                toastr.info('Espere por favor...', 'Actualizando...');
                canchasService.updHorario(horarioUpd).then(function (data) {
                    
                    if(data.estado=='Ok'){
                    
                        toastr.success('Se ha creado correctamente', 'Exito');
                        $(document).ready(function(){ $('.modal').modal('close');});
                        canchasService.get($routeParams.id).then(function (data) {
                            $scope.data=data.horario;
                            $(document).ready(function(){ $('.modal').modal('close');});
                        },function (error, data) {
                             console.log(error);
                        });
                        
                    }else{
                        toastr.warning(data.mensaje, 'Alerta');
                    }
                },
                function (argument) {
                    $scope.load=false;
                    toastr.error('Ha ocurrido un error grave', 'Error'); 
                      
                });
            }
        }
    
      
    };
    
    $scope.delete=function (id) {
        SweetAlert.swal({
           title: "Estás seguro?",
           text: "Deseas eliminar este horario?",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           confirmButtonText: "Si",
           cancelButtonText: "No",
           closeOnConfirm: true,
           closeOnCancel: true }, 
            function(isConfirm){ 
               if (isConfirm) {
                  toastr.warning('Espere por favor...', 'Eliminando...');
                    canchasService.delHorario(id).then(function (data) {
                        toastr.success('Eliminado correctamente', 'Exito');
        
                        canchasService.get($routeParams.id).then(function (data) {
                            $scope.data=data.horario;
                        },function (error, data) {
                             console.log(error);
                        });
                    },
                    function (argument) {
                        $scope.load=false;
                        toastr.error('Ha ocurrido un error grave', 'Exito'); 
                          
                    });
                 
               } 
            });
    };
   
    
}])
.controller('galleryCancha', ['$scope', 'canchasService', '$routeParams', 'SweetAlert','toastr', function ($scope, canchasService, $routeParams, SweetAlert, toastr) {
    $scope.load=false;
    $scope.url='https://canchas-deportivas-jeferbustamante.c9users.io/canchas/';
    canchasService.get($routeParams.id).then(function (data) {
        var i;
        $scope.fotos=[];
        $scope.fotos_id=[];
        $scope.data=data.fotos;
        for(i=0; i<data.fotos.length; i++){
            //console.log(data.fotos_lugar[i]);
            $scope.fotos[i]=$scope.url+data.fotos[i].ruta;
            $scope.fotos_id[i]=data.fotos[i].id;
        } 
        $scope.load=true;
    },function (error, data) {
         console.log(error);
    });
    
    
    $scope.deleteImg=function (id) {
        SweetAlert.swal({
           title: "Estás seguro?",
           text: "Deseas eliminar esta imagen?",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           confirmButtonText: "Si",
           cancelButtonText: "No",
           closeOnConfirm: true,
           closeOnCancel: true }, 
            function(isConfirm){ 
               if (isConfirm) {
                  toastr.warning('Espere por favor...', 'Eliminando...');
                    canchasService.deleteGallery(id).then(function (data) {
                        toastr.success('Eliminado correctamente', 'Exito');
                        canchasService.get($routeParams.id).then(function (data) {
                            var i;
                            $scope.fotos=[];
                            $scope.fotos_id=[];
                            $scope.data=data.fotos;
                            for(i=0; i<data.fotos.length; i++){
                                //console.log(data.fotos_lugar[i]);
                                $scope.fotos[i]=$scope.url+data.fotos[i].ruta;
                                $scope.fotos_id[i]=data.fotos[i].id;
                            } 
                            $scope.load=true;
                        },function (error, data) {
                             console.log(error);
                        });     
                                                
                    },
                    function (argument) {
                        $scope.load=false;
                        toastr.error('Ha ocurrido un error grave', 'Exito'); 
                          
                    });
                 
               } 
            });
    }
    
    $scope.send=function (cancha) {
        cancha.cancha_id=$routeParams.id
        
        toastr.info('Espere por favor...', 'Creando...');
        canchasService.postGallery(cancha).then(function (data) {
            cancha.fotos={};
            toastr.success('Se ha creado correctamente', 'Exito');
            $(document).ready(function(){ $('.modal').modal('close');});
            
            canchasService.get($routeParams.id).then(function (data) {
                var i;
                $scope.fotos=[];
                $scope.fotos_id=[];
                $scope.data=data.fotos;
                for(i=0; i<data.fotos.length; i++){
                    //console.log(data.fotos_lugar[i]);
                    $scope.fotos[i]=$scope.url+data.fotos[i].ruta;
                    $scope.fotos_id[i]=data.fotos[i].id;
                } 
                $scope.load=true;
                
            },function (error, data) {
                 console.log(error);
            });
        },
        function (argument) {
            $scope.load=false;
            toastr.error('Ha ocurrido un error grave', 'Exito'); 
              
        });
    }
    
    
}])
.controller('editCancha', ['$scope', 'lugarService', '$routeParams','encargadoService','toastr','$location','canchasService', function ($scope, lugarService, $routeParams, encargadoService, toastr, $location, canchasService) {
    $scope.load=false;
    $scope.url='https://canchas-deportivas-jeferbustamante.c9users.io/canchas/';
    
    
   lugarService.all().then(function (data) {
        $scope.data=data;
        $scope.load=true;
        $(document).ready(function(){ $('select').material_select();});
    },function (error, data) {
         console.log(error);
    });
    
    canchasService.disponible().then(function (data) {
        $scope.disp=data;
        $scope.load=true;
        $(document).ready(function(){ $('select').material_select();});
    },function (error, data) {
         console.log(error);
    });
    
    
    canchasService.get($routeParams.id).then(function (data) {
        $scope.cancha=data;
        $scope.cancha.disponibilidad_id=""+$scope.cancha.disponibilidad_id;
        $scope.cancha.lugar_id=""+$scope.cancha.lugar_id;
        var i=0;
        $scope.cancha.chips = [];
        for (i; i<data.chip.length; i++){
            $scope.cancha.chips[i]={'tag': data.chip[i].caracteristica};
        }
        //console.log($scope.cancha.chips);
        $(document).ready(function(){  $('.chips').material_chip({data: $scope.cancha.chips});});
        $scope.load=true;
        $(document).ready(function(){ $('select').material_select();});
    },function (error, data) {
         console.log(error);
    });
    
    
    $scope.update=function (cancha) {
       
        if(cancha.chips[0]){
            if(cancha.chips[0].tag ){
                cancha.edit=false;  
            }else{
                cancha.edit=true;  
            }
            
        }else{
            cancha.edit=true;  
        }
        
        toastr.info('Espere por favor...', 'Creando...');
        canchasService.put(cancha).then(function (data) {
            cancha={};
            toastr.success('Se ha creado correctamente', 'Exito');
            $location.path('/admin/canchas'); 
        },
        function (argument) {
            $scope.load=false;
            toastr.error('Ha ocurrido un error grave', 'Error'); 
              
        });
        //console.log(data)
    };
    
}])
.controller('deleteCancha', ['$scope', 'canchasService', '$routeParams', 'toastr', '$location', function ($scope, canchasService, $routeParams, toastr, $location) {
    $scope.load=false;
    canchasService.get($routeParams.id).then(function (data) {
        $scope.data=data;
        $scope.load=true;
    },function (error, data) {
         console.log(error);
    });
    
    
    $scope.delete=function () {
        
        toastr.warning('Espere por favor...', 'Eliminando...');
        canchasService.delete($routeParams.id).then(function (data) {
            toastr.success('Eliminado correctamente', 'Exito');
            $location.path('/admin/canchas'); 
        },
        function (argument) {
            $scope.load=false;
            toastr.error('Ha ocurrido un error grave', 'Exito'); 
              
        });
    };
    
}]);
