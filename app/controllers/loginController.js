angular.module('login.controller', [])
.controller('loginController', ['$scope', 'toastr', '$location','loginService','$base64','$cookieStore','CONFIG','ROLES', function ($scope, toastr, $location, loginService, $base64, $cookieStore, CONFIG, ROLES) {
    
    
    if($cookieStore.get('userDat') && $cookieStore.get('token')){
        var decoded=$base64.decode($cookieStore.get('userDat'));
        var user= JSON.parse(decoded);
        CONFIG.ROL_CURRENT_USER= user.rol_id;
         if(CONFIG.ROL_CURRENT_USER == 1)
    		{
    			$location.path(ROLES.ADMIN.PATH);
    		}
    		else if(CONFIG.ROL_CURRENT_USER == 2)
    		{
    			$location.path(ROLES.OWNER.PATH);
    		}
    		else if(CONFIG.ROL_CURRENT_USER == 3)
    		{
    			$location.path(ROLES.USER.PATH);
    		}
    	
        
    }
    $scope.logged=function (user) {
        loginService.auth(user).then(function (data) {
     
           $cookieStore.put('token',data.token);
           var encoded = $base64.encode(JSON.stringify(data.user[0]));
           $cookieStore.put('userDat', encoded);
           CONFIG.ROL_CURRENT_USER= data.user[0].rol_id;
           
            if(CONFIG.ROL_CURRENT_USER == 1)
    		{
    			$location.path(ROLES.ADMIN.PATH);
    		}
    		else if(CONFIG.ROL_CURRENT_USER == 2)
    		{
    			$location.path(ROLES.OWNER.PATH);
    		}
    		else if(CONFIG.ROL_CURRENT_USER == 3)
    		{   
    		    loginService.userClient(data.user[0].id).then(function (user) {
                    data.user[0].cliente=user[0];
                    encoded = $base64.encode(JSON.stringify(data.user[0]));
                    $cookieStore.put('userDat', encoded);
                   
                }, function () {});
    			$location.path(ROLES.USER.PATH);
    		}
           //var decoded = $base64.decode(encoded);
           //console.log(JSON.parse(decoded), encoded)
          
           
        }, function (error) {
            //console.log(error.data.error)
        });
    };
}])
.controller('logoutController', ['$scope', 'toastr','$location','$cookieStore', 'CONFIG', function ($scope, toastr, $location, $cookieStore, CONFIG) {
    
  
            $cookieStore.remove('token');
            $cookieStore.remove('userDat');
            CONFIG.ROL_CURRENT_USER=0;
            $location.path("/login");
            
    
}])
.controller('resgisterController', ['$scope', 'toastr','$location','registerService',function($scope, toastr, $location, registerService) {
    
    $scope.registrar=function (user) {
       
       
        user.tipo=1;
        
        toastr.info('Espere por favor...', 'Registrando...');
        registerService.post(user).then(function (data) {
            
            user.apellido="";
            user.mail="";
            user.nombre="";
            user.password="";
            user.telefono="";
            user.username="";
            toastr.success('Tu usuario ha sido registrado, inicia sesion y disfruta de nuestos servicios', 'Exito');
            $location.path('/login'); 
           
        }, 
        function (error) {
            user.password="";
            toastr.error('Ha ocurrido un error grave', 'Exito'); 
       });
       
    };
}]);