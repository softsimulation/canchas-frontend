angular.module('usuarios.controller', [])



.controller('listUsuario', ['$scope', 'usuariosService',function ($scope, usuariosService) {
    $scope.load=false;
    usuariosService.list().then(function (data) {
        $scope.data=data;
        $scope.load=true;
    }, 
    function (error, data) {
        console.log(error);
    });
    
}])
.controller('viewUsuario', ['$scope', 'usuariosService', '$routeParams', function ($scope, usuariosService, $routeParams) {
    $scope.load=false;
    usuariosService.get($routeParams.id).then(function (data) {
        $scope.data=data;
        $scope.load=true;
    },function (error, data) {
         console.log(error);
    });
    
}])

.controller('editUsuario', ['$scope', 'usuariosService', '$routeParams', 'toastr', '$location', function ($scope, usuariosService, $routeParams, toastr, $location) {
    $scope.load=false;
    usuariosService.get($routeParams.id).then(function (data) {
        $scope.user=data;
        $scope.load=true;
    },function (error, data) {
         console.log(error);
    });
    
    
    $scope.update=function (user) {
        
        toastr.info('Espere por favor...', 'Actualizando...');
        usuariosService.put(user).then(function (data) {
            toastr.success('Se ha actualizado correctamente', 'Exito');
            $location.path('/admin/user'); 
        },
        function (argument) {
            $scope.load=false;
            toastr.error('Ha ocurrido un error grave', 'Exito'); 
              
        });
    };
    
}])
.controller('createUsuario', ['$scope', 'usuariosService', '$routeParams', 'toastr', '$location', 'adminService', function ($scope, usuariosService, $routeParams, toastr, $location, adminService) {
   
    $scope.bandera=false;
    
    adminService.list().then(function (data) {
        $scope.admin=data;
        //console.log(data)
        $(document).ready(function(){ $('select').material_select();});
    }, function (error, data) {
        console.log(error);
    });
    
    $scope.changeSelect=function (id) {
        if(id==2){
            $scope.bandera=true;
        }
        else{
            $scope.bandera=false;
        }
    };
    
    $scope.create=function (user) {
        console.log(user)
        //user.tipo=2;
        //user.admin_id=1;
        toastr.info('Espere por favor...', 'Creando...');
        usuariosService.post(user).then(function (data) {
            user={};
            toastr.success('Se ha creado correctamente', 'Exito');
            $location.path('/admin/user'); 
        },
        function (argument) {
            $scope.load=false;
            toastr.error('Ha ocurrido un error grave', 'Exito'); 
              
        });
    };
    
}])
.controller('deleteUsuario', ['$scope', 'usuariosService', '$routeParams', 'toastr', '$location', function ($scope, usuariosService, $routeParams, toastr, $location) {
    $scope.load=false;
    usuariosService.get($routeParams.id).then(function (data) {
        $scope.data=data;
        $scope.load=true;
    },function (error, data) {
         console.log(error);
    });
    
    
    $scope.delete=function () {
        
        toastr.warning('Espere por favor...', 'Eliminando...');
        usuariosService.delete($routeParams.id).then(function (data) {
            toastr.success('Eliminado correctamente', 'Exito');
            $location.path('/admin/user'); 
        },
        function (argument) {
            $scope.load=false;
            toastr.error('Ha ocurrido un error grave', 'Exito'); 
              
        });
    };
    
}])

;
