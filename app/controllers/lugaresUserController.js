angular.module('lugares.user.controller', [])

.controller('lugaresController',['$scope', 'lugarService',function($scope, lugarService) {
   
   $scope.url="https://canchas-deportivas-jeferbustamante.c9users.io/canchas/";
    $scope.load=false;
    $(document).ready(function(){$('.carousel.carousel-slider').carousel({fullWidth: true});});
    lugarService.all().then(function (data) {
        
        $scope.items=data;
         $scope.load=true;
        
    }, 
    function (error, data) {
        
    });
}])

.controller('lugarController',['$scope', 'lugarService', '$routeParams',function($scope, lugarService, $routeParams) {
    $scope.url="https://canchas-deportivas-jeferbustamante.c9users.io/canchas/";
     
   
    lugarService.get($routeParams.id).then(function (data) {
        $scope.item=data;
        $scope.fotos=data.fotos_lugar;
        $scope.load=true;
    },function (error, data) {
        
        
    });
    
    
    lugarService.comentarios($routeParams.id).then(function (data) {
        $scope.comentarios=data;
        $scope.load=true;
         
    },function (error, data) {
        
    });
    
   
}])

.controller('canchasLugarController',['$scope', 'lugarService', '$routeParams',function($scope, lugarService, $routeParams) {
   $scope.url="https://canchas-deportivas-jeferbustamante.c9users.io/canchas/";
   $scope.lugar_id=$routeParams.id;
   
    
    lugarService.canchasLugar($routeParams.id).then(function (data) {
         $scope.items=data;
         $scope.fotos=data.fotosr;
         $scope.load=true;
    },function (error, data) {
        
    });
}])

.controller('canchaController',['$scope', '$routeParams', 'canchasService',function($scope, $routeParams, canchasService) {
    $scope.url="https://canchas-deportivas-jeferbustamante.c9users.io/canchas/";
   
    
    canchasService.get($routeParams.id).then(function (data) {
         $scope.item=data;
         $scope.load=true;
         
    },function (error, data) {
        
    });
}])

.controller('busquedaAvanzadaController', function($scope, lugarService, $timeout, caracteristicasServices, searchService, $base64, $cookieStore ) {
    
    $scope.month = ['Enero ', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    $scope.monthShort = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
    $scope.weekdaysFull = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    $scope.weekdaysLetter = ['D', 'L', 'M', 'M', 'J', 'V', 'S'];
    $scope.today = 'Hoy';
    $scope.clear = 'Limpiar';
    $scope.close = 'Cerrar';  
    $(document).ready(function(){ $('.modal').modal();});
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          
            $scope.latitud=position.coords.latitude;
            $scope.longitud=position.coords.longitude;
            $scope.$apply();
            //console.log($scope.lugar)
        }, function() {
            console.log('No se puede obtener su posicion');
        });
    } else {
        // Browser doesn't support Geolocation
        console.log('Navegador sin geolocation');
    }    
    
    
    $scope.dista=0;
    //$scope.fecha=new Date();
    $scope.url="https://canchas-deportivas-jeferbustamante.c9users.io/canchas/";
    //$scope.selection=[];
    

    
    caracteristicasServices.get().then(function (data) {
        $scope.dat=data;
        $(document).ready(function(){ $('select').material_select();});
        
        
    }, function () {
        
    });
    
    $scope.search=function (dista, fecha, selection, inicio, fin) {
        
        var data={
            //'distancia':dista,
            //'fecha':fecha.getFullYear()+"-"+addZero((fecha.getMonth()+1))+"-"+addZero(fecha.getDate()),
            //'caracteristicas':selection,
            'latitud':$scope.latitud,
            'longitud':$scope.longitud,
            //'horaInicio':addZero(inicio.getHours())+":"+addZero(inicio.getMinutes()),
            //'horaFin':addZero(fin.getHours())+":"+addZero(fin.getMinutes()),
        };
       
        if(dista>0){
            data.distancia=dista;
        }

        if((typeof fecha !== 'undefined') 
        && (typeof inicio !== 'undefined')
        && (typeof fin !== 'undefined')){
            data.fecha=fecha;
            data.horaInicio=inicio;
            data.horaFin=fin;
        }
        if(typeof selection !== 'undefined') {
            data.caracteristicas=selection;
        }
        console.log(data);
       
        searchService.advanced(data).then(function (data) {
            $scope.items=data;
            console.log(data)
            
        }, function () {
            
        });
        
    };
    
    
    
    function addZero(i) {
        if (i < 10) {i = "0" + i;}
        return i;
    }    
})