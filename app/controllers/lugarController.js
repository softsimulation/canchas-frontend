angular.module('lugar.controller', [])

.controller('listLugar', ['$scope', 'lugarService',function ($scope, lugarService) {
    $scope.load=false;
    lugarService.all().then(function (data) {
        
        $scope.data=data;
        $scope.load=true;
        
    }, 
    function (error, data) {
        console.log(error);
    });
    
}])
.controller('viewLugar', ['$scope', 'lugarService', '$routeParams', function ($scope, lugarService, $routeParams) {
    $scope.load=false;
    
    lugarService.get($routeParams.id).then(function (data) {
        $scope.data=data;
        $scope.load=true;
    },function (error, data) {
         console.log(error);
    });
    
}])
.controller('createLugar', ['$scope', 'lugarService', '$routeParams', 'toastr', '$location', 'encargadoService','adminService','localizacionService', function ($scope, lugarService, $routeParams, toastr, $location, encargadoService,adminService, localizacionService) {
   
    $scope.lugar={};
    
    $scope.load=false;
    $(document).ready(function(){ $('.modal').modal({ dismissible: true, // Modal can be dismissed by clicking outside of the modal
                                                       opacity: .5, // Opacity of modal background
                                                       inDuration: 300, // Transition in duration
                                                       outDuration: 200, // Transition out duration
                                                       startingTop: '4%', // Starting top style attribute
                                                       endingTop: '10%',
                                                    });
    });
    
    
    localizacionService.listDptos(1).then(function (data) {
    
        $scope.dptos=data;
        $(document).ready(function(){ $('select').material_select();});
    }, function (error, data) {
        console.log(error);
    });
    
    
    $scope.municiposload=function(id){
       
       localizacionService.listMpios(id).then(function (data) {
            $scope.mpios=data;
            $(document).ready(function(){ $('select').material_select();});
        }, function (error, data) {
            console.log(error);
        }); 
    }; 
      
    $scope.encagargadosLoad=function (id) {
        encargadoService.list(id).then(function (data) {
            $scope.data=data;
            $(document).ready(function(){ $('select').material_select();});
        }, function (error, data) {
            console.log(error);
        });
    };
    
    
    adminService.list().then(function (data) {
    
        $scope.admin=data;
        $(document).ready(function(){ $('select').material_select();});
    }, function (error, data) {
        console.log(error);
    });
    
    $scope.map=function () {
       
        $('#modal1').modal('open');
        var map=null;
        var markers=[];
        var latLng = new google.maps.LatLng(11.233108999999999, -74.192919);
        var mapOptions = {
                  center: latLng,
                  zoom:13,
            };
        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        
        map.addListener('click', function(event) {
            deleteMarkers();
             var markerLatLng = addMarker(event.latLng);
             $scope.lugar.latitud=markerLatLng.lat();
             $scope.lugar.longitud=markerLatLng.lng();
             $scope.$apply();
        });
        
        function addMarker(location) {
          var marker = new google.maps.Marker({
            position: location,
            map: map
          });
          markers.push(marker);
          return marker.getPosition();
          
        }
    
        function setMapOnAll(map) {
          for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
          }
        }
        
        function clearMarkers() {
          setMapOnAll(null);
        }
    
        function deleteMarkers() {
          clearMarkers();
          markers = [];
        }
    };
    
    $scope.setPosition=function () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
              
                $scope.lugar.latitud=position.coords.latitude;
                $scope.lugar.longitud=position.coords.longitude;
                $scope.$apply();
                //console.log($scope.lugar)
            }, function() {
                console.log('No se puede obtener su posicion');
            });
          } else {
            // Browser doesn't support Geolocation
            console.log('Navegador sin geolocation');
          }
    };
    
    $scope.create=function (lugar) {
        
        //lugar.admin_id=1;
        toastr.info('Espere por favor...', 'Creando...');
        lugarService.post(lugar).then(function (data) {
            lugar={};
            toastr.success('Se ha creado correctamente', 'Exito');
            $location.path('/admin/lugares'); 
        },
        function (argument) {
            $scope.load=false;
            toastr.error('Ha ocurrido un error grave', 'Exito'); 
              
        });
       // console.log(lugar)
    };
    
    
}])

.controller('galleryLugar', ['$scope', 'lugarService', '$routeParams', 'toastr','$location', 'SweetAlert', function ($scope, lugarService, $routeParams, toastr,$location, SweetAlert) {
    $scope.load=false;
    $scope.url='https://canchas-deportivas-jeferbustamante.c9users.io/canchas/';
    $(document).ready(function(){ $('.modal').modal({ dismissible: true, // Modal can be dismissed by clicking outside of the modal
                                                       opacity: .5, // Opacity of modal background
                                                       inDuration: 300, // Transition in duration
                                                       outDuration: 200, // Transition out duration
                                                       startingTop: '4%', // Starting top style attribute
                                                       endingTop: '10%',
                                                    });
    });
    
    
    lugarService.get($routeParams.id).then(function (data) {
        var i;
        $scope.fotos=[];
        $scope.fotos_id=[];
        $scope.data=data.fotos_lugar;
        for(i=0; i<data.fotos_lugar.length; i++){
            //console.log(data.fotos_lugar[i]);
            $scope.fotos[i]=$scope.url+data.fotos_lugar[i].ruta;
            //var x={'url':$scope.url+data.fotos_lugar[i].ruta, 'id':data.fotos_lugar[i].id};
            $scope.fotos_id[i]=data.fotos_lugar[i].id;
        } 
        
        
        $scope.load=true;
        
    },function (error, data) {
         console.log(error);
    });
    
    $scope.deleteImg=function (id) {
        SweetAlert.swal({
           title: "Estás seguro?",
           text: "Deseas eliminar esta imagen?",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           confirmButtonText: "Si",
           cancelButtonText: "No",
           closeOnConfirm: true,
           closeOnCancel: true }, 
            function(isConfirm){ 
               if (isConfirm) {
                  toastr.warning('Espere por favor...', 'Eliminando...');
                    lugarService.deleteGallery(id).then(function (data) {
                        toastr.success('Eliminado correctamente', 'Exito');
                        lugarService.get($routeParams.id).then(function (data) {
                            var i;
                            $scope.fotos=[];
                            $scope.fotos_id=[];
                            $scope.data=data.fotos_lugar;
                            for(i=0; i<data.fotos_lugar.length; i++){
                                //console.log(data.fotos_lugar[i]);
                                $scope.fotos[i]=$scope.url+data.fotos_lugar[i].ruta;
                                //var x={'url':$scope.url+data.fotos_lugar[i].ruta, 'id':data.fotos_lugar[i].id};
                                $scope.fotos_id[i]=data.fotos_lugar[i].id;
                            } 
                            
                            
                            $scope.load=true;
                            
                        },function (error, data) {
                             console.log(error);
                        });       
                                                
                    },
                    function (argument) {
                        $scope.load=false;
                        toastr.error('Ha ocurrido un error grave', 'Exito'); 
                          
                    });
                 
               } 
            });
    };
    
    $scope.send=function (lugar) {
        lugar.lugar_id=$routeParams.id;
        
        toastr.info('Espere por favor...', 'Creando...');
        lugarService.postGallery(lugar).then(function (data) {
            lugar.fotos={};
            toastr.success('Se ha creado correctamente', 'Exito');
            $(document).ready(function(){ $('.modal').modal('close');});
            
            lugarService.get($routeParams.id).then(function (data) {
                var i;
                $scope.fotos=[];
                $scope.fotos_id=[];
                $scope.data=data.fotos_lugar;
                for(i=0; i<data.fotos_lugar.length; i++){
                    //console.log(data.fotos_lugar[i]);
                    $scope.fotos[i]=$scope.url+data.fotos_lugar[i].ruta;
                    //var x={'url':$scope.url+data.fotos_lugar[i].ruta, 'id':data.fotos_lugar[i].id};
                    $scope.fotos_id[i]=data.fotos_lugar[i].id;
                } 
                
                
                $scope.load=true;
                
            },function (error, data) {
                 console.log(error);
            });
        },
        function (argument) {
            $scope.load=false;
            toastr.error('Ha ocurrido un error grave', 'Exito'); 
              
        });
    }
    
}])


.controller('editLugar', ['$scope', 'lugarService', '$routeParams','encargadoService','toastr','$location','adminService','localizacionService', function ($scope, lugarService, $routeParams, encargadoService, toastr, $location,adminService, localizacionService) {
    $scope.load=false;
    $scope.url='https://canchas-deportivas-jeferbustamante.c9users.io/canchas/';
    $(document).ready(function(){ $('.modal').modal({ dismissible: true, // Modal can be dismissed by clicking outside of the modal
                                                       opacity: .5, // Opacity of modal background
                                                       inDuration: 300, // Transition in duration
                                                       outDuration: 200, // Transition out duration
                                                       startingTop: '4%', // Starting top style attribute
                                                       endingTop: '10%',
                                                    });
    });
    
    
    localizacionService.listDptos(1).then(function (data) {
    
        $scope.dptos=data;
        $(document).ready(function(){ $('select').material_select();});
    }, function (error, data) {
        console.log(error);
    });
    
    
    $scope.municiposload=function(id){
       
       localizacionService.listMpios(id).then(function (data) {
            $scope.mpios=data;
            $(document).ready(function(){ $('select').material_select();});
        }, function (error, data) {
            console.log(error);
        }); 
    }; 
    
    
    
    $scope.encagargadosLoad=function (id) {
        encargadoService.list(id).then(function (data) {
            $scope.data=data;
            $(document).ready(function(){ $('select').material_select();});
        }, function (error, data) {
            console.log(error);
        });
    };
    
    
    adminService.list().then(function (data) {
    
        $scope.admin=data;
        $(document).ready(function(){ $('select').material_select();});
    }, function (error, data) {
        console.log(error);
    });
    
    $scope.map=function () {
       
        $('#modal1').modal('open');
        var map=null;
        var markers=[];
        var latLng = new google.maps.LatLng(11.233108999999999, -74.192919);
        var mapOptions = {
                  center: latLng,
                  zoom:13,
            };
        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        
        map.addListener('click', function(event) {
            deleteMarkers();
             var markerLatLng = addMarker(event.latLng);
             $scope.lugar.latitud=markerLatLng.lat();
             $scope.lugar.longitud=markerLatLng.lng();
             $scope.$apply();
        });
        
        function addMarker(location) {
          var marker = new google.maps.Marker({
            position: location,
            map: map
          });
          markers.push(marker);
          return marker.getPosition();
          
        }
    
        function setMapOnAll(map) {
          for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
          }
        }
        
        function clearMarkers() {
          setMapOnAll(null);
        }
    
        function deleteMarkers() {
          clearMarkers();
          markers = [];
        }
    };
    
 
    $scope.setPosition=function () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
              
                $scope.lugar.latitud=position.coords.latitude;
                $scope.lugar.longitud=position.coords.longitude;
                 $scope.$apply();
                //console.log($scope.lugar)
            }, function() {
                console.log('No se puede obtener su posicion');
            });
          } else {
            // Browser doesn't support Geolocation
            console.log('Navegador sin geolocation');
          }
    };
    
    
    lugarService.get($routeParams.id).then(function (data) {
        $scope.lugar=data;
        $scope.lugar.encargados_id=""+data.encargados_lugar[0].encargados_id;
        
        localizacionService.munucipio(data.mpios_ciudades_id).then(function (datas) {
            $scope.lugar.dptos_estados_id=""+datas.dptos_estados.id;
            $scope.lugar.mpios_ciudades_id=""+data.mpios_ciudades_id;
            $(document).ready(function(){ $('select').material_select();});
        }, function (error, data) {
            console.log(error);
        }); 
        $scope.load=true;
        $(document).ready(function(){ $('select').material_select();});
    },function (error, data) {
         console.log(error);
    });
    
    
    $scope.update=function (lugar) {
       //lugar.admin_id=1;
        toastr.info('Espere por favor...', 'Creando...');
        lugarService.put(lugar).then(function (data) {
            lugar={};
            toastr.success('Se ha creado correctamente', 'Exito');
            $location.path('/admin/lugares'); 
        },
        function (argument) {
            $scope.load=false;
            toastr.error('Ha ocurrido un error grave', 'Exito'); 
              
        });
    };
    
}]).
controller('deleteLugar', ['$scope', 'lugarService', '$routeParams', 'toastr', '$location', function ($scope, lugarService, $routeParams, toastr, $location) {
    $scope.load=false;
    lugarService.get($routeParams.id).then(function (data) {
        $scope.data=data;
        $scope.load=true;
    },function (error, data) {
         console.log(error);
    });
    
    
    $scope.delete=function () {
        
        toastr.warning('Espere por favor...', 'Eliminando...');
        lugarService.delete($routeParams.id).then(function (data) {
            toastr.success('Eliminado correctamente', 'Exito');
            $location.path('/admin/lugares'); 
        },
        function (argument) {
            $scope.load=false;
            toastr.error('Ha ocurrido un error grave', 'Exito'); 
              
        });
    };
    
}])
.controller('lugarCanchasController', ['$scope', 'lugarService', '$routeParams',function ($scope, lugarService, $routeParams) {
    $scope.load=false;
    lugarService.canchasLugar($routeParams.id).then(function (data) {
        $scope.data=data;
        $scope.load=true;
    }, 
    function (error, data) {
        console.log(error);
    });
    
}])
;
