angular.module('equipos.controller', [])

.controller('equiposController', ['$scope','$route' ,'equiposService', '$location','$base64','$cookieStore', 'SweetAlert', 'toastr','$timeout',function($scope,$route ,equiposService, $location,$base64, $cookieStore, SweetAlert,toastr, $timeout) {
    var decoded=$base64.decode($cookieStore.get('userDat'));
    var user= JSON.parse(decoded);
    $scope.month = ['Enero ', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    $scope.monthShort = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
    $scope.weekdaysFull = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    $scope.weekdaysLetter = ['D', 'L', 'M', 'M', 'J', 'V', 'S'];
    $scope.today = 'Hoy';
    $scope.clear = 'Limpiar';
    $scope.close = 'Cerrar';
    
    $scope.url="https://canchas-deportivas-jeferbustamante.c9users.io/canchas/";
    $scope.target="";
    $scope.target2="";
    $scope.nameTarget="";
    $scope.load=false;
    $(document).ready(function(){ $('.modal').modal();});
    
    equiposService.get(user.cliente.id).then(function (data) {
        $scope.items=data;
        $scope.load=true;
    },function () {
        toastr.error('Ha ocurrido un error grave', 'Error');   
    });
    
    equiposService.getInactivos(user.cliente.id).then(function (data) {
        $scope.inactivos=data;
    },function () {
        toastr.error('Ha ocurrido un error grave', 'Error');   
    });
    
    $scope.actionSheet=function(id, name) {$scope.target=id; $scope.nameTarget=name;};
    
    $scope.actionSheet2=function(id) {$scope.target2=id;};
    
    $scope.delEquipo=function (id) {
        $(document).ready(function(){ $('.modal').modal('close');});
        SweetAlert.swal({
        title: "Estás seguro?",
        text: "Deseas desactivar este equipo?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true }, 
        function(isConfirm){ 
            if (isConfirm) {
                toastr.info('Espere por favor...', 'Desactivando...');
                equiposService.delete(id).then(function (data) {
                    SweetAlert.swal("Exito!", "Tu equipo se ha desactivado", "success");
                    $route.reload();
                },function () {
                    SweetAlert.swal("Error", "Ha ocurrido un error grave", "error");
                });
            } 
        });
    };
    
    $scope.restore=function (id) {
        $(document).ready(function(){ $('.modal').modal('close');});
        SweetAlert.swal({
        title: "Estás seguro?",
        text: "Deseas restaurar este equipo?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true }, 
        function(isConfirm){ 
            if (isConfirm) {
                toastr.info('Espere por favor...', 'Restaurando...');
                equiposService.restoreEquipo(id).then(function (data) {
                    SweetAlert.swal("Exito!", "Tu equipo se ha restaurado", "success");
                    $route.reload();
                     
                },function () {
                    SweetAlert.swal("Error", "Ha ocurrido un error grave", "error");
                });
             
            } 
        });
    };
    
    $scope.deleteDef=function (id) {
        var data={'id':id};
        $(document).ready(function(){ $('.modal').modal('close');});
        SweetAlert.swal({
        title: "Estás seguro?",
        text: "Deseas eliminar de forma permanente este equipo?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true }, 
        function(isConfirm){ 
            if (isConfirm) {
                toastr.info('Espere por favor...', 'Eliminando...');
                equiposService.deleteEquipoDef(data).then(function (data) {
                    SweetAlert.swal("Exito!", "Tu equipo se ha elominado", "success");
                    $route.reload();
                    
                },function () {
                    SweetAlert.swal("Error", "Ha ocurrido un error grave", "error");
                });
             
            } 
        });
    };
    
    $scope.link=function (caso, id) {
        $(document).ready(function(){ $('.modal').modal('close');});
        $timeout(function(){
           switch(caso){
                case 1:
                    $location.path("/user/jugadores/"+id);
                break;
                
                case 2:
                    $location.path("/user/equipos/edit/"+id);
                break;
                
                
            }  
             
        },500);
        
    };
    
    $scope.add=function (data) {
        var jugar={
            cliente_id:user.cliente.id,
            miEquipo_id:$scope.target,
            fecha:data.date,
            
        };
        
        equiposService.quieroJugar(jugar).then(function (data) {
            if(data.estado==='ok'){
                toastr.success('Se ha creado correctamente', 'Exito');
                $(document).ready(function(){ $('.modal').modal('close');});
                
            }else{
                toastr.warning(data.mensaje, 'Alerta');
            }
        },function () {
            toastr.error('Ha ocurrido un error grave', 'Error'); 
        });
        
        
    };
}])

.controller('addEquipoController',[ '$scope', 'equiposService', '$base64', '$cookieStore', '$location','toastr', '$location',function($scope, equiposService, $base64, $cookieStore, $location, toastr) {
    var decoded=$base64.decode($cookieStore.get('userDat'));
    var user= JSON.parse(decoded);
   
    $scope.add=function (equipo) {
        
        equipo.cliente_id=user.cliente.id;
        toastr.info('Espere por favor...', 'Creando...');
        equiposService.post(equipo).then(function (data) {
            toastr.success('Se ha creado correctamente', 'Exito');
  		    $location.path('/user/equipos');
        }, function () {
            toastr.error('Ha ocurrido un error grave', 'Error'); 
        });
      
    };
}])

.controller('editEquipoController',['$scope', 'equiposService', '$routeParams', '$location', 'toastr', function($scope, equiposService, $routeParams,$location, toastr) {
    $scope.load=false;
    $scope.url="https://canchas-deportivas-jeferbustamante.c9users.io/canchas/";
    equiposService.equipo($routeParams.id).then(function (data) {
        $scope.equipo=data;
        $scope.equipo.logo={'url':$scope.url+$scope.equipo.ruta_logo};
        $scope.load=true;   
    },function () {
        toastr.error('Ha ocurrido un error grave', 'Error');    
    });
   
  
    $scope.edit=function (equipo) {
        toastr.info('Espere por favor...', 'Actualizando...');
        equiposService.put(equipo).then(function (data) {
            toastr.success('Se ha actualizado correctamente', 'Exito');
            $location.path('/user/equipos');
            
        },function () {
            toastr.error('Ha ocurrido un error grave', 'Error');   
        });
      
    };
}])

.controller('rivalesController',['$scope', 'equiposService', '$routeParams', '$base64', '$cookieStore', 'toastr', function($scope,  equiposService, $routeParams, $base64,$cookieStore, toastr  ) {
    var decoded=$base64.decode($cookieStore.get('userDat'));
    var user= JSON.parse(decoded);
    $scope.load=false;
    $scope.url="https://canchas-deportivas-jeferbustamante.c9users.io/canchas/";
    
    equiposService.jugar(user.cliente.id).then(function (data) {
        $scope.items=data;
        $scope.load=true;   
    },function () {
        toastr.error('Ha ocurrido un error grave', 'Error');   
    });
}])

.controller('retarController',['$scope','lugarService', '$timeout', 'equiposService', '$location', '$base64', '$cookieStore', '$routeParams', '$route', 'userService', 'toastr', function($scope,lugarService, $timeout, equiposService, $location, $base64, $cookieStore, $routeParams, $route, userService, toastr ) {
    var decoded=$base64.decode($cookieStore.get('userDat'));
    var user= JSON.parse(decoded);
    $scope.url="https://canchas-deportivas-jeferbustamante.c9users.io/canchas/";
    
    
    equiposService.get(user.cliente.id).then(function (data) {
            $scope.miEquipo=data;
            
             
        },function () {
               
    });
    
    
    
    equiposService.equipo($routeParams.idEquipo).then(function (data) {
        $scope.equipo=data;
        userService.get(data.cliente_id).then(function (data) {
            $scope.usuario=data;
        },function () {
            toastr.error('Ha ocurrido un error grave', 'Error'); 
        });
         
        equiposService.getQuieroJugar($routeParams.idQuieroJugar).then(function (data) {
            $scope.quieroJugar=data[0];
        },function () {
            toastr.error('Ha ocurrido un error grave', 'Error'); 
        });
         
    },  function () {
        toastr.error('Ha ocurrido un error grave', 'Error'); 
    });
    
    lugarService.all().then(function (data) {
        $scope.lugares=data;
    },function (error, data) {
        toastr.error('Ha ocurrido un error grave', 'Error');  
    });
    
    $scope.add=function (lugar_id, miEquipo_retador_id) {
       
        var retar={
           'lugar_id':lugar_id,
           'cliente_id':user.cliente.id,
           'miEquipo_id':$routeParams.idEquipo,
           'estadoReto_id':1, 
           'quieroJugar_id':$routeParams.idQuieroJugar, 
           'miEquipo_retador_id':miEquipo_retador_id
        };
        toastr.info('Espere por favor...', 'Retando...');
        equiposService.retar(retar).then(function (data) {
            if(data.estado==='ok'){
                toastr.success('Se ha enviado tu reto al equipo rival.', 'Exito');
                $location.path("/user/misRetos");
            }else{
                toastr.warning(data.mensaje, 'Alerta');
            }
            
        },function () {
            toastr.error('Ha ocurrido un error grave', 'Error'); 
        });       
    };  
}])

.controller('misRetosController',['$scope','lugarService', 'userService', '$timeout', 'equiposService', '$location', '$base64','$cookieStore', 'SweetAlert', 'toastr', '$route' ,function($scope,lugarService, userService, $timeout, equiposService, $location, $base64, $cookieStore, SweetAlert, toastr, $route) {
    var decoded=$base64.decode($cookieStore.get('userDat'));
    var user= JSON.parse(decoded);
    $scope.url="https://canchas-deportivas-jeferbustamante.c9users.io/canchas/";
    
    equiposService.misRetos(user.cliente.id).then(function (data) {
        $scope.items=data;
       
    },function () {
        toastr.error('Ha ocurrido un error grave', 'Error'); 
    });
    
    function updateReto(id, estadoReto_id){
        
        var reto={'id':id, 'estadoReto_id': estadoReto_id};
        
        
        equiposService.updateReto(reto).then(function (data) {
            $scope.items=data;
            SweetAlert.swal("Exito!", "Se ha actualizado el reto", "success");
            $route.reload();
            
       
        },function () {
            toastr.error('Ha ocurrido un error grave', 'Error');     
        });   
    }
    
    $scope.update=function (reto, estadoReto_id) {
        if(estadoReto_id==1){
            SweetAlert.swal({
            title: "Estás seguro?",
            text: "Deseas reactivar este reto?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true }, 
            function(isConfirm){ 
                if (isConfirm) {
                    toastr.info('Espere por favor...', 'Reactivando...');
                    updateReto(reto, estadoReto_id);
                } 
            });
        }
        else if(estadoReto_id==4){
            SweetAlert.swal({
            title: "Estás seguro?",
            text: "Deseas cancelar este reto?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true }, 
            function(isConfirm){ 
                if (isConfirm) {
                    toastr.info('Espere por favor...', 'Cancelando...');
                    updateReto(reto, estadoReto_id);
                } 
            });
        }
    };
    
    
}])

.controller('meRetaController',['$scope', 'lugarService', 'userService', '$timeout', 'equiposService', '$location', '$base64','$cookieStore', 'SweetAlert', 'toastr', '$route', function($scope, lugarService, userService, $timeout, equiposService, $location, $base64, $cookieStore, SweetAlert, toastr, $route) {
    var decoded=$base64.decode($cookieStore.get('userDat'));
    var user= JSON.parse(decoded);
    $scope.url="https://canchas-deportivas-jeferbustamante.c9users.io/canchas/";
    
    
    equiposService.meReta(user.cliente.id).then(function (data) {
        $scope.items=data;
        
    }, function () {
        
    });
    
    
    
    function updateReto(id, estadoReto_id){
        
        
        var reto={'id':id, 'estadoReto_id': estadoReto_id};
        
        equiposService.updateReto(reto).then(function (data) {
            $scope.items=data;
            SweetAlert.swal("Exito!", "Se ha actualizado el reto", "success");
            $route.reload();
       
        }, function () {
            
        });
    
        
    }
    $scope.update=function (reto, estadoReto_id) {
        if(estadoReto_id==2){
            SweetAlert.swal({
            title: "Estás seguro?",
            text: "Deseas aceptar este reto?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true }, 
            function(isConfirm){ 
                if (isConfirm) {
                    toastr.info('Espere por favor...', 'Reactivando...');
                    updateReto(reto, estadoReto_id);
                } 
            });
        }
        else if(estadoReto_id==3){
            SweetAlert.swal({
            title: "Estás seguro?",
            text: "Deseas rechazar este reto?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true }, 
            function(isConfirm){ 
                if (isConfirm) {
                    toastr.info('Espere por favor...', 'Cancelando...');
                    updateReto(reto, estadoReto_id);
                } 
            });
        }
    };
    
    
      
   
}])


.controller('jugadoresController', function($scope,  userService, $timeout,  equiposService, $routeParams, $route) {
    $scope.id=$routeParams.id;  
    $scope.url="https://canchas-deportivas-jeferbustamante.c9users.io/canchas/";
    $scope.load=false;
        
    
    equiposService.equipo($scope.id).then(function (data) {
         
         $scope.equipo=data;
          $scope.load=true;
         
    },function () {
        
        /*var alertPopup =$ionicPopup.alert({
            title: '¡Error!',
            template: 'Ha ocurrido un error.',
            okType:'button-stable'
        });*/

        /*alertPopup.then(function(res) {
              ionic.Platform.exitApp(); // stops the app
        });  */
    });
    
    
    equiposService.jugadoresEquipo($scope.id).then(function (data) {
        
         $scope.items=data;
          $scope.load=true;
        
    },function () {});
    
    equiposService.jugadoresInvitados($scope.id).then(function (data) {
         $scope.invitados=data;
        console.log(data)
    },function () {});
    

    function deleteInvitado(id) {
       
        
        var dat={'id':id};
        
        equiposService.destroyInvitado(dat).then(function (data) {
            //$scope.items=data;
            /*$ionicLoading.hide();
            
            $ionicPopup.alert({
                title: '¡Exito!',
                template: 'Se ha cancelado la invitacio.',
                okType:'button-stable'
            });*/
            $route.reload();
            
       
        }, function () {
           
            /*var alertPopup =$ionicPopup.alert({
                title: '¡Error!',
                template: 'Ha ocurrido un error.',
                okType:'button-stable'
            });

            alertPopup.then(function(res) {
                  ionic.Platform.exitApp(); // stops the app
            });*/
        });  
    }
    
  
    
    
    /*$scope.actionSheet=function  (id) {
        $ionicActionSheet.show({
            titleText: '¡Elige una acción!',
            buttons: [
              { text: '<i class="icon ion-close-circled assertive"></i>Cancelar Invitación' },
              
              
              
            ],
            cancelText: '<i class="icon ion-android-close assertive"></i>Cancelar',
            cancel: function() {},
            buttonClicked: function(index) {
                 $ionicPopup.confirm({
                                   title: 'Cancelara la invitacion',
                                   template: '¿Seguro que desea hacerlo?', 
                                   buttons: [
                                       {text:'No', type: 'button-stable',},
                                       {
                                        text:'Si',type: 'button-stable', 
                                        onTap: function(e) {
                                            deleteInvitado(id);
                                            
                                        }  
       
                                       }
                                       ]
                                });
                  
             return true;
            },
        });
    }; 
*/
  /*   $scope.actionSheet2=function  (jugadores_id) {
        $ionicActionSheet.show({
            titleText: '¡Elige una acción!',
            buttons: [
              { text: '<i class="icon ion-close-circled assertive"></i>Eliminar del equipo' },
              
            ],
            cancelText: '<i class="icon ion-android-close assertive"></i>Cancelar',
            cancel: function() {},
            buttonClicked: function(index) {
           
                $ionicPopup.confirm({
                   title: 'Abandonará  este  equipo',
                   template: '¿Seguro que desea hacerlo?', 
                   buttons: [
                       {text:'No', type: 'button-stable',},
                       {
                        text:'Si',type: 'button-stable', 
                        onTap: function(e) {deleteInvitado(jugadores_id)}  

                       }
                       ]
                });
                   
             
                return true;
            },
        });
    };  */ 
})

