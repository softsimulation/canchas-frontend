angular.module('profile.user.controller', [])

.controller('profileController',['$scope', 'profileUserService',  '$timeout', '$base64', '$cookieStore', '$route', 'toastr',function($scope, profileUserService,  $timeout, $base64, $cookieStore, $route, toastr) {
    var decoded=$base64.decode($cookieStore.get('userDat'));
    var datas= JSON.parse(decoded);
    $scope.user=datas;
    $(document).ready(function(){ $('.modal').modal();}); 
    
    $scope.update=function  (user) {
       
        toastr.info('Espere por favor...', 'Actualizando...');
        profileUserService.update(user).then(function  (data) {
            datas=$scope.user;
            
            
            $cookieStore.remove('userDat');
            var encoded = $base64.encode(JSON.stringify(datas));
            $cookieStore.put('userDat', encoded);
            
            $(document).ready(function(){ $('.modal').modal('close');});
            toastr.success('Se ha actualizado tu perfil', 'Exito');
            $timeout(function(){$route.reload();},500);
            
        }); 
    };
}])