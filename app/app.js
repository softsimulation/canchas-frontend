
angular.module('app', ['ngRoute',
                       'ngCookies',
                       'angular-jwt',
                       'base64',
                       'colorpicker.module',
                       'toastr',
                       'angular-input-stars',
                       'ui.materialize',
                       'encargado.controller',
                       'encargado.services',
                       'lugar.services',
                       'lugar.controller',
                       'canchas.controller',
                       'canchas.services',
                       'reserva.controller',
                       'reserva.owner.controller',
                       'reserva.services',
                       'localizacion.services',
                       'directives',
                       'angular-lightbox',
                       'oitozero.ngSweetAlert',
                       'admin.services',
                       'admin.controller',
                       'canchas.owner.controller',
                       'usuarios.controller',
                       'usuarios.services',
                       'login.controller',
                       'login.services',
                       'lugares.user.controller',
                       'equipos.controller',
                       'equipos.services',
                       'reserva.user.controller',
                       'reserva.user.services',
                       'pagos.services',
                       'profile.user.service',
                       'profile.user.controller'
                       
                        ])
                        
.constant('CONFIG', {
    APIURL: "https://canchas-deportivas-jeferbustamante.c9users.io/canchas/public/",
    TEMPLATE_DIR:"templates/",
	ROL_CURRENT_USER: 0
  })

 
.constant('ROLES', {
	ADMIN: {
		ROL:1,
		PATH:"/admin/lugares"
	},
	OWNER: {
		ROL:2,
		PATH:"/owner/canchas"
	},
	USER: {
		ROL:3,
		PATH:"/user/lugares"
	}, 
	INDEX:{
	    ROL:0,
		PATH:"/login"
	}
})

.config(function(toastrConfig, $rootScopeProvider) {
  angular.extend(toastrConfig, {
    allowHtml: false,
    closeButton: false,
    closeHtml: '<button>&times;</button>',
    extendedTimeOut: 1000,
    iconClasses: {
      error: 'toast-error',
      info: 'toast-info',
      success: 'toast-success',
      warning: 'toast-warning'
    },  
    messageClass: 'toast-message',
    onHidden: null,
    onShown: null,
    onTap: null,
    progressBar: true,
    tapToDismiss: true,
    templates: {
      toast: 'directives/toast/toast.html',
      progressbar: 'directives/progressbar/progressbar.html'
    },
    timeOut: 3000,
    titleClass: 'toast-title',
    toastClass: 'toast'
  });
  
  
})

.config(function ($routeProvider, $locationProvider, ROLES) {
    $routeProvider
    
    
        /**Login y Registro**/
         .when('/login', {
            templateUrl: 'templates/index/login.html',
            controller: 'loginController',
            data:{
                authorized: [ROLES.INDEX.ROL]
            }
        })
        
        
        .when('/register', {
            templateUrl: 'templates/index/register.html',
            controller:'resgisterController',
            data:{
                authorized: [ROLES.INDEX.ROL]
            }
        })
        .when('/logout', {
            templateUrl: 'templates/index.html',
            controller: 'logoutController'
            
        })
        
        
        /**Superadmin **/
        .when('/admin', {
            templateUrl: 'templates/index.html',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/lugares', {
            templateUrl: 'templates/admin/lugarTable.html',
            controller:'listLugar',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        
        .when('/admin/lugares/add', {
            templateUrl: 'templates/admin/lugarAdd.html',
            controller:'createLugar',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/lugares/edit/:id', {
            templateUrl: 'templates/admin/lugarEdit.html',
            controller:'editLugar',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/lugares/delete/:id', {
            templateUrl: 'templates/admin/lugarDel.html',
            controller:'deleteLugar',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/lugares/view/:id', {
            templateUrl: 'templates/admin/lugarView.html',
            controller:'viewLugar',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/lugares/gallery/:id', {
            templateUrl: 'templates/admin/lugarGallery.html',
            controller:'galleryLugar',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
         .when('/admin/lugares/:id/canchas', {
            templateUrl: 'templates/admin/canchasTable.html',
            controller:'lugarCanchasController',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/canchas/', {
            templateUrl: 'templates/admin/canchasTable.html', 
            controller: 'listCanchas',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/canchas/add', {
            templateUrl: 'templates/admin/canchasAdd.html', 
            controller: 'createCancha',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/canchas/view/:id', {
            templateUrl: 'templates/admin/canchasView.html', 
            controller: 'viewCanchas',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/canchas/delete/:id', {
            templateUrl: 'templates/admin/canchasDel.html', 
            controller: 'deleteCancha',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/canchas/edit/:id', {
            templateUrl: 'templates/admin/canchasEdit.html', 
            controller: 'editCancha',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/canchas/gallery/:id', {
            templateUrl: 'templates/admin/canchasGallery.html', 
            controller: 'galleryCancha',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/canchas/horario/:id', {
            templateUrl: 'templates/admin/canchaHorario.html', 
            controller: 'createHorario',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/lugares/:id/reserva', {
            templateUrl: 'templates/admin/reservaTable.html', 
            controller: 'getReserva',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/lugares/:id/reserva/historico', {
            templateUrl: 'templates/owner/reservaTableHisto.html', 
            controller: 'getReservaHisto',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        
        .when('/admin/reservaReto/edit/:id', {
            templateUrl: 'templates/admin/reservaRetoView.html', 
            controller: 'editrReservaReto',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        
        .when('/admin/reserva/add', {
            templateUrl: 'templates/admin/reservaAdd.html', 
            controller: 'createReserva',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/reserva/listado', {
            templateUrl: 'templates/admin/reservaList.html', 
            controller: 'listReserva',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/reserva/edit/:id', {
            templateUrl: 'templates/admin/reservaView.html', 
            controller: 'editReserva',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/user', {
            templateUrl: 'templates/admin/usuariosTable.html',
            controller:'listUsuario',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/user/add', {
            templateUrl: 'templates/admin/usuariosAdd.html',
            controller:'createUsuario',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/user/edit/:id', {
            templateUrl: 'templates/admin/usuariosEdit.html',
            controller:'editUsuario',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        .when('/admin/user/delete/:id', {
            templateUrl: 'templates/admin/usuariosDel.html',
            controller:'deleteUsuario',
            data:{
                authorized: [ROLES.ADMIN.ROL]
            }
        })
        
        
        
        /**Dueño de las canchas**/
        .when('/owner', {
            templateUrl: 'templates/index.html',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
        .when('/owner/canchas/', {
            templateUrl: 'templates/owner/canchasTable.html', 
            controller: 'listOwnerCanchas',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
        .when('/owner/canchas/add', {
            templateUrl: 'templates/owner/canchasAdd.html', 
            controller: 'createOwnerCancha',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
        .when('/owner/canchas/view/:id', {
            templateUrl: 'templates/owner/canchasView.html', 
            controller: 'viewOwnerCanchas',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
        .when('/owner/canchas/delete/:id', {
            templateUrl: 'templates/owner/canchasDel.html', 
            controller: 'deleteOwnerCancha',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
        .when('/owner/canchas/edit/:id', {
            templateUrl: 'templates/owner/canchasEdit.html', 
            controller: 'editOwnerCancha',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
        .when('/owner/canchas/gallery/:id', {
            templateUrl: 'templates/owner/canchasGallery.html', 
            controller: 'galleryOwnerCancha',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
        .when('/owner/canchas/horario/:id', {
            templateUrl: 'templates/owner/canchaHorario.html', 
            controller: 'createOwnerHorario',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
        .when('/owner/encargados', {
            templateUrl: 'templates/owner/encargadoTable.html',
            controller: 'listEncargado',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
        .when('/owner/encargados/add', {
            templateUrl: 'templates/owner/encargadoAdd.html',
            controller:'createEncargado',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
        .when('/owner/encargados/edit/:id', {
            templateUrl: 'templates/owner/encargadoEdit.html',
            controller:'editEncargado',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
         .when('/owner/encargados/delete/:id', {
            templateUrl: 'templates/owner/encargadoDel.html',
            controller:'deleteEncargado',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
        .when('/owner/lugares/:id/reserva', {
            templateUrl: 'templates/owner/reservaTable.html', 
            controller: 'getOwnerReserva',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
        .when('/owner/lugares/:id/reserva/historico', {
            templateUrl: 'templates/owner/reservaTableHisto.html', 
            controller: 'getOwnerReservaHisto',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
        
        .when('/owner/reserva/add', {
            templateUrl: 'templates/owner/reservaAdd.html', 
            controller: 'createOwnerReserva',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
        .when('/owner/reserva/listado', {
            templateUrl: 'templates/owner/reservaList.html', 
            controller: 'listOwnerCanchasReserva',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
        .when('/owner/reserva/edit/:id', {
            templateUrl: 'templates/owner/reservaView.html', 
            controller: 'editOwnerReserva',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
        .when('/owner/reservaReto/edit/:id', {
            templateUrl: 'templates/owner/reservaRetoView.html', 
            controller: 'editOwnerReservaReto',
            data:{
                authorized: [ROLES.OWNER.ROL]
            }
        })
        
        /**Usuarios clientes**/
        .when('/user/lugares', {
            templateUrl: 'templates/user/lugares.html', 
            controller: 'lugaresController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        .when('/user/lugar/:id', {
            templateUrl: 'templates/user/lugar.html', 
            controller: 'lugarController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        
        .when('/user/lugar/:id/canchas', {
            templateUrl: 'templates/user/canchas.html', 
            controller: 'canchasLugarController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        .when('/user/cancha/:id', {
            templateUrl: 'templates/user/cancha.html', 
            controller: 'canchaController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        .when('/user/equipos', {
            templateUrl: 'templates/user/equipos.html', 
            controller: 'equiposController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        
        .when('/user/equipos/add', {
            templateUrl: 'templates/user/equiposAdd.html', 
            controller: 'addEquipoController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        
        .when('/user/equipos/edit/:id', {
            templateUrl: 'templates/user/editEquipo.html', 
            controller: 'editEquipoController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        .when('/user/misReservas', {
            templateUrl: 'templates/user/misReservas.html', 
            controller: 'misReservasController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        .when('/user/misReservas/historicos', {
            templateUrl: 'templates/user/reservaHistorico.html', 
            controller: 'historicoController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        
        .when('/user/reservar', {
            templateUrl: 'templates/user/reservar.html', 
            controller: 'reservarController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        
        .when('/user/reserva/:id', {
            templateUrl: 'templates/user/reserva.html', 
            controller: 'reservaController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        
        .when('/user/retoReservado/:id', {
            templateUrl: 'templates/user/retoReservado.html', 
            controller: 'retoReservadoController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        
        .when('/user/profile', {
            templateUrl: 'templates/user/profile.html', 
            controller: 'profileController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        
        .when('/user/rivales', {
            templateUrl: 'templates/user/equiposRivales.html', 
            controller: 'rivalesController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        .when('/user/retar/:idEquipo/:idQuieroJugar', {
            templateUrl: 'templates/user/retar.html', 
            controller: 'retarController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        .when('/user/misRetos', {
            templateUrl: 'templates/user/misRetos.html', 
            controller: 'misRetosController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        
        .when('/user/reservaReto/:id', {
            templateUrl: 'templates/user/reservaReto.html', 
            controller: 'reservaRetoController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        
        .when('/user/meReta', {
            templateUrl: 'templates/user/meReta.html', 
            controller: 'meRetaController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        
        .when('/user/busquedaAvanzada', {
            templateUrl: 'templates/user/busquedaAv.html', 
            controller: 'busquedaAvanzadaController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        
        .when('/user/jugadores/:id', {
            templateUrl: 'templates/user/jugadores.html', 
            controller: 'jugadoresController',
            data:{
                authorized: [ROLES.USER.ROL]
            }
        })
        
        .otherwise({
	        redirectTo: '/login'
	    });
    
        $locationProvider.html5Mode({
          enabled: true,
          requireBase: false
        });
})


.run(["$rootScope", "$location", "CONFIG", "ROLES", '$cookieStore','$base64', function($rootScope, $location, CONFIG, ROLES, $cookieStore, $base64)
{
	$rootScope.$on('$routeChangeStart', function (event, next) 
	{
	        if(!$cookieStore.get('token') || !$cookieStore.get('userDat')){
        
                 $cookieStore.remove('token');
                 $cookieStore.remove('userDat');
                 CONFIG.ROL_CURRENT_USER=0;
                 //$location.path("/login");
                         
            }else{
                var decoded=$base64.decode($cookieStore.get('userDat'));
                var user= JSON.parse(decoded);
                CONFIG.ROL_CURRENT_USER= user.rol_id;
                
            }
    			    
       
            if (next.data !== undefined) 
            {
                
                if(next.data.authorized.indexOf(CONFIG.ROL_CURRENT_USER) !== -1)
    			{
    				
    			}
    			else
    			{
    			    
    				if(CONFIG.ROL_CURRENT_USER == 1)
    				{
    					$location.path(ROLES.ADMIN.PATH);
    				}
    				else if(CONFIG.ROL_CURRENT_USER == 2)
    				{
    					$location.path(ROLES.OWNER.PATH);
    				}
    				else if(CONFIG.ROL_CURRENT_USER == 3)
    				{
    					$location.path(ROLES.USER.PATH);
    					
    				}else if(CONFIG.ROL_CURRENT_USER == 0){
    				    
    				    $location.path(ROLES.INDEX.PATH);
    				}
                    
    			}
            }
            
        
    });
}]);


